# DevOps practical training documentation

During the practical training part of the Dev2Ops course in late 2019 we worked on developing a fully automated (noOps if you will...) CiCD pipeline for a mobile application that was created with React Native. The objective was to make the pipeline so automated that practically (and preferably) no manual interaction with deployment would need to be done.

For information and instructions on how to setup and use various tools used with this project check from their individual sections.

1. [ReactNative (JS framework for Androis and iOS)](Documentation/1-awesome-project.md)
1. [Commitizen (npm package)](Documentation/2-commitizen.md)
1. [Semantic Release (npm package)](Documentation/3-semantic-release.md)
1. [Ci (GitLab & CircleCi)](Documentation/4-ci.md)
1. [FastLane (npm package)](Documentation/5-fastlane.md)
1. [CodePush (App Center)](Documentation/6-codepush.md)
1. [Detox (testing tool)](Documentation/7-detox.md)
1. [Good To Know](Documentation/8-good-to-know.md)
1. [Links (collection of links to guides and tools used in this project)](Documentation/links.md)
1. [Tools (short rundown on each tool used in this project)](Documentation/tools.md)

Since the development of the app itself was not our task, we used a basic React Native example app that we called CiTestApp. To be able to test Detox screenshot taking we added a simple button that added and removed a single image from the main app view. We also used a Closed Beta track on Google Play store to verify that the app actually does work and is being uploaded to Google Play Store.

The project was mainly done using a Mac as it allowed for running both Android and iOS development environments as well as build and emulate both.

    For a full rundown on how to setup an example RN project with CICD pipeline, check awesome-project.md

---

## Where are we now

    CircleCI pipeline works fully; Pushing to alpha initiates update / build creation to alpha deployment / track. Merge + push to master initiates Beta build creation and codePush update promotion. Promote lane in fastlane promotes update on Alpha deployment, so any update needs to go through push to Alpha and then merge to Beta.

    GitLabCI pipeline works for alpha-branch, but requires re-configuration to enable merge based activation, currently merge is skipped, likely due to [skip ci] -message in the commit created after semanti-release is activated, re-running pipeline for master via GitLab UI DOES WORK as intended, automatic activation DOES NOT. Fastlane configuration is identical to CircleCI setup, only difference is in GitLabCI config.

## CI Workflow

`Git cz` initiates `commitizen` plugin that enforces that commits are standardized as per conventional commit specification.

This is done to later allow for detection of Major, Minor, Fix or other updates (e.g CodePush could act based on these).

### Update-lane

`Git push` on alpha-branch initiates Ci pipeline that:

1. Detects which branch are the actions executed on and runs update-lane in fastlane.

1. Creates a keystorefile in android/app based on env-var saved as env in CircleCi.

1. Creates `android/app/src/main/assets/appcenter-config.json` file to use with App Center & Codepush based on env saved in CircleCi.

1. Add deployment key into `android/app/src/main/res/values/strings.xml` based on env saved in CircleCi.

1. Restores gradle/ruby/npm cache.

1. Installs missing ruby dependencies (based on Gemfile).

1. Installs missing npm dependencies (based on package.json).

1. Saves gradle/ruby/npm cache.

   - Not all gradle cache files are cached, this is by design and likely can be reconfigured.

1. Runs `npx semantic-release`-plugin

   - Creates a CHANGELOG.md file int the root folder with latest commit notes.
   - Creates a new release with new/missing commit notes and if necessary (as per semver/conventional commit) bumps the version number in package.json as well as adds a vX.X.X tag to the Git release.
   - semantic-release is only initiated for alpha-branch.

1. Runs either `update`-lane in fastlane.
   - `update`-lane is used to detect which update is needed (major.minor.patch) and to release the update either on codePush Alpha-deployment or create a new build and upload it to Google Play Store Alpha-track.

### Promote-lane

1. `git checkout master` + `git merge alpha` with `git push` initiates a promote-lane for master branch ONLY.

1. Detects which update is required
   - Major = new build for Google Play Beta-track
     - The last number of the 6-digit version code is used to represent which track it is for, this is Google Play specific config because Google expects version code on a "higher" track to be higher.
   - Minor / Patch = promotes the latest Alpha-deployment update on codePush to Beta-deployment.

---

## Important notes

- Application version is based on package.json version, which is assigned and increased with semantic-release.
- Version code is used by Google Play Store and can be any integer value, as long as any new build has a higher value.
- Codepush updates can be promoted from one deployment to another
- Google Play Store track releases can be promoted to a higher track
- IF a new build is required for any track, it needs to have higher version code than the previous build (Production > Beta > Alpha > Internal)

---

## ENVs

- ALPHA_KEY = alpha deployment key (codepush)
- BETA_KEY = beta deployment key (codepush)
- APPCENTERTOKEN = appcenter token created with an account that is logged in to appcenter (`appcenter tokens create -d "TOKEN_NAME"`), required to allow for headless logins to appcenter (from CI) **! DOES NOT EXPIRE !**
- APPSECRET = Encoded appsecret.json (assigned by codepush when app is added to codepush). Decrypted in CI
- GH_TOKEN = Github token for semantic-release
- JSON_KEY = An encoded json containing Google Play Store credentials. Used in Fastlane Appfile to provide fastlane Google credentials.
- KEYSTORE = An encoded keystore file, decoded in CI, needed for Google Play Store
- KEY_PWD = keystore aliaksen password
- STORE_PWD = keystore file password

---

## Creating builds locally

To create builds locally (and for app to be locally compatible with codePush), you will need to do the following:

- add codepush deployment key to stringx.xml
- add pwd env vars
  - KEY_PWD=citestapp
  - STORE_PWD=citestapp
- make sure that version in android/app/build.gradle is correct
- add appcenter-config.json (from codepush) to android/app/src/main/assets
- make sure that the keyfile is in the correct folder
- run ./gradlew clean in android dir
- run ./gradlew assembleRelease to create new build locally

## Testing & screenshots with detox

At the moment, detox part is working only when executed locally, this is due to limitations of CircleCi. Unfortunately it appears that CircleCi can't emulate android devices using x86 architecture, which means that;

- a) some other service needs to be used for android emulation / screenshot creation.
- b) screenshots and tests would be done manually or using a external runner.
- c) android builds would be done by non x86 architecture for eg. armapi versions of android, which would lead to long build times.

To implement detox into Ci, the following needs to be added to the ci file.

To create a testable build for detox: `npm run detox_build`

To take screenshots of the emulated device based on tests defined in `e2e/firstTest.spec.js`-file add the following: `npm run detox_screenshots`. This would create screenshots in the app_root/artifacts -directory

To copy files to correct folder to be picked up by Google Play store add :`find artifacts/android.emu.release*/*phoneScreenshots -name "*.png" -exec cp {} android/fastlane/metadata/android/fi-FI/images/phoneScreenshots \;`

## Things left ~~to do~~ undone

- ~~Create Ci for main app~~
- ~~Create customized commands in commitizen to initialize codepush based on commit messages~~
- ~~Screenshot generation using CI~~ (CircleCi cant run x86).
- ~~Detox installation and screenshot generation.~~ (Done but not in CI)
- ~~iOS build and upload~~
- ~~graphQL integration tests~~

### Other notes

For convenience reasons, Appfile for Fastlane contains a quick check for json key data in env because locally it's present as a file but during ci it's' in the env.
