# Some good to know things (when setting this project up)

## Useful commands & tips

If you clone your git project and move it elsewhere, you might want to keep your already created tags to avoid fiddling with version and changelog information.

- use `git push --tags` to add tags after you´ve cloned the project.

Fix symbolic links after installing/uninstalling packages

- `yarn install`

Its a good idea to remove the node_modules folder and run npm install every now and then. It might fix some package related issues, specially if you install/uninstall several new packages.

- `rm -rf node_modules && npm install`

Packages installed via package.json (for example in Ci) will likely not run with usual commands, simply add execution call in the scripts section of the package.json.

- eg. `"my_command": "random_npm_program_thats_included_in_package.json_dependency`

To clean/remove all apk/aab packages

- `./gradlew clean` or `cd android && ./gradlew clean`

To build a release package (android/app/build/outputs/apk/release)

- `./gradlew assembleRelease` or `cd android && ./gradlew assembleRelease`

List system images available in Android SDK manager. Note, these images are not installed on the system by default, instead run `sdkmanager name_of_image` to install whichever image you need.

- `sdkmanager --list --verbose | grep system-images`
