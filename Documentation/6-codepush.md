# Codepush (Microsoft App Center)

## What is Codepush

Codepush is a tool/service by Microsoft (now moved to App Center, still free...). It allows updating of React Native and Apache Cordova applications directly by pushing "small" updates like JS, HTML, CSS and images without rebuilding the application. This allows for applications to act more like a website than a traditional smartphone app providing developers with ability to react to react to events practically immediately.

## How to setup Codepush/AppCenter

Setting Codepush up requires installation of Codepush npm packages as well as "CodePush-ifying" your app. The latter would essentially mean that the react-native-part of the application would be running through CodePush.

### App Center

Before you can even test codepush, you will need to add the application to appcenter.ms. Login and add your app (you can use github credentials), once you get the getting started page, follow the instructions.

![App Center getting started page](../src/appcenter_getting_started.PNG)

`npm install appcenter appcenter-cli appcenter-analytics appcenter-crashes`

    Note!
    App Center guide instructs installing appcenter-cli with -g to make it globally available, this can be useful but is likely not possible on CI. It can be installed "normally" but would require creating custom commands/scripts to run appcenter commands, alternatively you can use npx.

    You can also install it at the same time with other appcenter tools as you will be using it on CI anyway so it should be added to dependencies.

To install appcenter-cli globally just add the -g option: `npm install -g appcenter-cli`

Once AppCenter tools are installed, you can create a new build and run the app via emulator, you should quite soon see some data in the Analytics part of App Center.

### CodePushifying your app

Before you can start pushing smalle update via App Center, you will need to do small changes to the entry point file of the application as well as to MainApplication.java to initialize codePush

This guide is meant for android version only, for iOS take a look at Microsofts App Center docs <https://docs.microsoft.com/en-us/appcenter/distribution/codepush/react-native#getting-started>.

Install react-native-code-push package: `npm install --save react-native-code-push`

To run react-native app taking advantage of its immediate updates, you first need to link react-native and react-native-code-push:

`react-native link react-native-code-push`

Alternatively, you can accomplish the same step manually (you might also want to check what the link command actually did). Check the android Setup part of MS App Center docs for more details: <https://docs.microsoft.com/en-us/appcenter/distribution/codepush/react-native#android-setup>.

Finally, to connect the example react native app to codepush, you can use this code to encapsulate the entire application add this to **_application_root/index.js_** (app entry point), you will still need to do the linking <https://docs.microsoft.com/en-us/appcenter/distribution/codepush/react-native#plugin-usage>:

    import codePush from 'react-native-code-push';
    const CodePushedApp = codePush(App);
    AppRegistry.registerComponent(appName, () => CodePushedApp);

After completing the previous steps you should be able to update any JS, HTML, CSS or image changes to your app by running `appcenter codepush release-react -a your_user/YOUR_APP -d DEPLOYMENT -t TARGETED_VERSION`

For more details on available options and parameters, check App Center docs.

## Useful commands & tips

- Check the App Center Overview page to see the app_secret and initial instructions.

- To login/register `code-push register`

- `appcenter codepush deployment list -a APP_NAME --displayKeys` Will show generated deployment keys for the chosen app.

- `appcenter codepush release-react -a YOUR_USER/CiTestApp -d Beta -t 1.3.28`

Options explained:

    -a = user/appname
    -d = Track
    -t = version e.g. as per json version
    --mandatory = update mandatory
    --description = adds a decription to the update

- `react-native link react-native-code-push` will create necessary links between react-native and react-native-code-push. These links can also be done manually.

- There is no need to install App Center globally in Ci, when used locally it might be a good option though.

- You can use `npx` to run appcenter commands if you did not install appcenter globally.

- `import codePush from 'react-native-code-push';` needs to be added to every script/file that will be using codePush directly.

- If you are using the newest React Native version, you might receive warnings and possibly errors when compiling a new build. Just revert to an older version to fix this. This guide was made based on React Native 0.61.4 while App Center only mentioned instructions up to 0.59. Most visible warning was regarding Android SDK build tools, which in our case were 29 but App Center plugins wanted to use version 27, this resulted in warning messages during the build.

- If you are testing the app locally via emulator, be sure to pass version number when you build your app, otherwise codepush will not be able to target your local app.

- To list all connected apps `code-push app ls`

- To check the status of pushed updates `code-push deployment ls YOUR_USER/CiTestApp`

- You might also be able to use code-push command directly without appcenter-cli installed, simply replace `appcenter codepush` with just `code-push`

- You can visit AppCenter > Distribute > CodePush page to check info regarding any of the updates you have made via codePush.

- If you are targeting both platforms (Android and iOS) it is recommended to create separate CodePush applications for each platform.

- To create an access-key for your app to be used when basic login/registration cant be used (CI) you will need to create an access token: `appcenter tokens create`

- You can connect/login to your account using --token option: `appcenter login --token YOUR_TOKEN_HERE`

- Check details of the app: `appcenter apps show --app YOUR_APP`

- To update metadata for EXISTING release: `appcenter codepush patch -a YOUR_APP -l REALEASE_VERSION_YUOU_WANT_TO_PATCH`

- It is possible to show some infromation provided / collected by codePush by using codePush.getUpdateMetadata(); (to provide info regarding AVAILABLE updates) and codePush.getConfiguration(); (to provide configuration info about CURRENT application). For more info refer to MS docs: <https://docs.microsoft.com/en-us/appcenter/distribution/codepush/react-native#api-reference> and <https://github.com/Microsoft/react-native-code-push/issues/395>

---

## Sources

### Codepush

[Microsoft App Center CLI (Microsoft @GitHub)](https://github.com/microsoft/appcenter-cli)

[App Center Push (Microsoft Docs)](https://docs.microsoft.com/en-us/appcenter/push/)

[CodePush Management CLI (Microsoft @GitHub)](https://github.com/microsoft/code-push/tree/master/cli)

[React Native Module for CodePush (Microsoft @GitHub)](https://github.com/microsoft/react-native-code-push#getting-started)

[Build the React Native Deployment Pipeline (Thedo)](https://blog.theodo.com/2019/04/react-native-deployment-pipeline/)

### React Native with CodePush

[React Native Code push (Microsoft GitHub)](https://github.com/microsoft/react-native-code-push)

[React Native Code push API (Microsoft GitHub)](https://github.com/microsoft/react-native-code-push/blob/master/docs/api-js.md)

[React Native code push (Microsoft GitHub)](https://github.com/microsoft/react-native-code-push/blob/master/Examples/create-app.js)

[React Native code push examples (Microsoft GitHub)](https://github.com/microsoft/react-native-code-push/tree/master/Examples/CodePushDemoApp/android/app)

[React Native CodePush (Medium)](https://medium.com/spritle-software/react-native-codepush-b86f0ea8432c)

[Get started with ‘CodePush’ (React-Native) (Medium)](https://medium.com/@rajanmaharjan/get-started-with-wonderful-technology-d838aafdc2d3)

[The right way to code push react native (Medium)](https://medium.com/fundbox-engineering/the-right-way-to-code-push-react-native-cba82d0f8ec9)

[React Native and Code Push example (Tyler Buchea Blog)](https://blog.tylerbuchea.com/react-native-and-code-push-example/)

[App Center react-native (Microsoft Docs)](https://docs.microsoft.com/en-us/appcenter/distribution/codepush/react-native)

[App Center codepush (Microsoft Docs)](https://docs.microsoft.com/en-us/appcenter/distribution/codepush/)

[React Native plugin - getting started (Microsoft Docs)](https://docs.microsoft.com/en-us/appcenter/distribution/codepush/react-native#getting-started)

[React Native plugin- android setup (Microsoft Docs)](https://docs.microsoft.com/en-us/appcenter/distribution/codepush/react-native#android-setup)

[React Native plugin - plugin useage(Microsoft Docs)](https://docs.microsoft.com/en-us/appcenter/distribution/codepush/react-native#plugin-usage)

[ (Microsoft GitHub)](https://github.com/Microsoft/code-push/tree/master/cli#access-keys)

[ (Microsoft GitHub)](https://github.com/microsoft/react-native-code-push/blob/master/docs/api-js.md#codepushsync)

[Codepush Integration in react native (LogicWind)](http://blog.logicwind.com/codepush-integration-in-react-native-app/)
