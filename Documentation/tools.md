# Tools

A short 'n' sweet introduction to the tools we used for this project.

## Code

Most of the coding is done with Visual Studio Code <https://code.visualstudio.com/>.

- Fastlane is ruby-based
- CI config-files are yaml-based but have some specific terminology
- React-Native is mostly Javascript
- Android is Java/Kotlin
- Detox is JavaScript
- some basic linux commands were also used

## ReactNative

React Native is a React based framework developed by Facebook <https://facebook.github.io/react-native/docs/getting-started.html>.

## Version & commit control

Right from the start we were instructed/advised to find a tool that would apply semver <https://semver.org/> and conventional commit <https://www.conventionalcommits.org/en/v1.0.0/> ideology/strategy. While semver introduces a very specific yet completely logical way of version management, conventional commit introduces a similar thinking to commits pushed via git.

## Semantic release

For semver we ended up with a Semantic Release plugin for npm <https://github.com/semantic-release/semantic-release>.

## Commitizen

For conventional commits we found a very convenient and easy to use and setup commitizen plugin for npm <https://github.com/commitizen/cz-cli>.

## Fastline

For this project we mainly focused on publishing an Android app to Google Play store. Fastlane-plugin <https://docs.fastlane.tools/> we were suggested to use works for both, iOS as well as Android and has extensive documentation as well.

## Process diagrams etc

Any process diagrams or other UML type drawings were created with Draw.io <https://www.draw.io/>.

## GitLab and GitHub

We ended up using both, GitLab <https://gitlab.com/> as well as GitHub <https://github.com/>. We started with GitLab but due to some limitation regarding app building we moved the project to GitHub. At the end of the project we moved it back to GitLab, this only required configuration of GitLabCI.

The main issue was that GitLab does not offer an iOS runner as part of their CI, they do offer an external runner service but in our case that was not sufficient. Unfortunately GitLab is also incompatible with CircleCI.

## CircleCI & GitLabCI

CircleCI <https://circleci.com/> and GitLabCI <https://gitlab.com/> are relatively similar but are currently (04.12.2019) incompatible. CircleCI is CI only service and has a wide array of available runners (free and paid). Extensive documentation can be found to integrate it with GitHub as well as other Git services (NOT GitLab). CircleCI yml configurations can be slightly easier to read.

GitLabCI configuration files are slightly more complex to read but offer very extensive configuration possibilities. Unfortunately GitLab does not currently offer its own iOS runners and require external iOS services, you can also configure your own runners.

## CodePush

CodePush <https://github.com/microsoft/code-push/tree/master/cli>, which has now been moved to Microsoft App Center <https://appcenter.ms/>, is a tool used to publish minor Cordova and ReactNative updates (JS, HTML, CSs and images).

The configuration requires relatively minor changes to android Java files and installation of couple npm packages. Documentation is extensive.

## Detox

Detox is a testing tool that can be used for React Native apps. Tests are written in JavaScript. To run the tests you will need to have device emulators setup.

CircleCI does not support running x86 android emulators.

We have not tested running android emulators on GitLabCI
