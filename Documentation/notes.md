# Devops practical training notes

    Things below are merely notes, newest ones go to the bottom

## Tavoite

Tavoitteena on luoda CICD-putki, jonka tarkoituksena on automatisoida mobiiliapplikaation julkaisu. Tämän saavuttamiseen käytetään allaolevia työkaluja, joilla on kaikilla oma roolinsa CICD-putkessa. Projektisssa käytetään semanttisen versioinnin (https://semver.org/) ja Conventional Commits (https://www.conventionalcommits.org) periaatteita.

## Commitizen

Jotta Semantic Release voi tehdä changelogin committien perusteella, käytetään Committizen työkalua valvomaan että commitit tehdään Conventional Commits-periaatteen mukaisesti, jolloin Semantic-Release voi analysoida commit historya standardinmukaisesti.

## Semantic Release

Semantic Release tekee changelogin.

Mikä määrittää sen miten Fastlane ajetaan? Ajetaanko Fastlane parametreillä vai lukeeko Fastlane changelogin, jonka pohjalta se päättää, onko tarkoitus käyttää Codepushia vai Google Play-päivitystä.

## Fastlane

Fastlane hoitaa mobiiliapplikaation julkaisun Microsoft CodePushilla tai päivittämällä sovelluksen Google Play Storeen. Patch-tyyppiset julkaisut tehdään CodePushilla ja minor-päivitykset tehdään Google Play Storeen.

### Muuta kivaa

https://docs.fastlane.tools/actions/supply/

https://blog.blueapron.io/release-notes-using-fastlane-fba80240dc4d

https://blog.usejournal.com/semantic-release-for-fastlane-781df4cf5888

## Other & old stuffs & notes

src/ kansioon kuvat ja scriptit
notes.md tiedostoon kerätyt ohjeet yms.
links.md tiedostoon kaikki mahdolliset linkit eri ohejisiin yms.

Mobiili sovelluksen CiCD ketju & automatisointi ettei tarvitse manuaalisesti luoda tai ladata mitään tiedostoja yms.

Changelog tiedosto jotta pystytään helposti seuraamaan päivityksiä
Hyödynnetään semantic versioning jotta pysyy järki versioissa
Hyödynnetään conventional commentsia jotta commentit saadaan hallintaan ja jotta ne on standardisoitu — samalla auttaa. changelogin sisällön hallintaa

fastlane
codepush service
googleplay realease track

## ToDo in a nutshell

- react native appi androidille
- kytketään commitizen
- kytketään fastlane
- apk paketti googleen app releases jollekkin trackille
- ensin fastlane semantic version / commit kanssa
- upload screenshits to google
- upload version info to google

## ToDo v2

DB

- GraphQL
- Bookshelf
- Knex

kysely
server.js
käyttäjän tiedot contextiin
kattoo schemasta miten resolvataan
kattoo data. soure
modeliin

React native

VS code extensions

- ruby
- lint
- other syntax hightlighters
- fastlane

fastlane

1. lint
2. testit npm scriptillä
3. build / deploy npm runn deploy tai fastlane

android studio SDK

code push
major manuaali
muut fastlane

---

## NOTES VKO 45

### Mitä meillä on nyt

- lokaali fastlane julkaisu googlen play storeen
- manuaalinen versionumero
- semantic release ciCD putkessa gitlabissa
- changelogin luonti gitlabiin CHANGELOG.md tiedostona
- commitizen commit viestien muotoilu
- appi julkaistu google play betassa
- check google version
- check current package.json version

#### Mitä tarvitaan vielä

- screenshottien luonti (fastlanella)
- screenshottien lataus google playihin
- fastlanen aktivointi cicd putkessa
- Ui testaus

#### Tutustu

- bitrise.io
- https://github.com/wix/Detox/blob/master/docs/Introduction.Android.md
- fastlane kontin teko ja käyttö

Ei tarvita koska releasenotesien pitää paremmin vastata käyttäjän tietotarvetta

- release notesin automaattinen haku & tiedoston luonti version mukaan
- release notesin lataus google playihin

kun macci huollossa:

- check fastlane on linux setup

### KE 6.11 TO DOOO

hakee commitit githubista
on kytketty googleen
ajaa analyze_commitin fastlanen pohjalta
asentaa npm
cache npm
asentaa tarvittavat gemit rubille
CACHE ei toimi rubylle???

### TO 7.11

semantic release githubille
eka versio muuttui 1.1.0 —> 1.0.0
oletettavasti koska eka github release
commitizen toimii

siirrettäessä gitlab repo githubiin pitää ottaa huomioon Releasien siirto
Tod. Näk tehtävä APIn kautta jotta kaikki mahd tieto säilyy

`git push —tags`

### PUUTTUU

upload to google cicd kautta

muista käyttää paikallista key.jsonia

muista määrittää google appi salasana yms circle cille

https://shift.infinite.red/simple-react-native-android-releases-319dc5e29605
https://facebook.github.io/react-native/docs/signed-apk-android.html?source=post_page-----319dc5e29605----------------------

set env variables VAR=adasd
export VAR=adfasdas

base64 keystore tiedosto

käytössä oleva keystore tiedosto on android/app kannsiossa

## TO DOOO 8.11

prosessikaavio uudesta fastfilesta
Google AAB
detox
codepush

---

## NOTES VKO 46

## KE 13.11

Vaikka CircleCI:ssä näyttää olevan asennettuna emulaattorin imaget ne eivät kuitenkaan ole eli ennen avdn konffausta ne pitää ladata, samalla on hyväksyttävä Android SDK lisenssit.

## TO 15.11 TOOOODOOOO

### Cache wars

- ruby cache OK
- npm cache OK
- gradle cache semi-OK

removing detox = dont yet

warningit pois... = waste of time bc. testapp

### Draw io kuva fastlanelle

- detox test & screenshots
- move to fastfile metadata
- git add .
- git cz
- git push
- Ci
- semantic release
- fastlane
- build
- upload to google play store
- manual release notes

### Muuta

- Ohjeistus screenshottien tekoon ja lataukseen yms paikallisella koneella
- Kuinka asentaa detox + kuinka luoda testit ja minne + screenshottien path ja minne fastlane kansiossa
- ios buildi CiTestApp?
- ios deployment vs google play deployment

#### Testit (varmaan jossain välissä pitää tehdä)

- graphQL integraatio testit
- lataa schema
- tsekkaa schemaa vastaan mobiili apissa käytössä queryt

## Errors and fixes

Error message during aab build:
apkNotificationMessageKeyBundleTargetsInvalidLanguage: The bundle targets unrecognized languages: fb

FIX by adding:

```
android {
   defaultConfig {
        ...
        resConfigs "en", "US"
        ...
   }
}
```

### update to latest fastlane

`sudo gem install fastlane`

`bundle install`

https://bundler.io/v1.10/man/bundle-install.1.html

https://gradle.org/install/

https://circleci.com/docs/2.0/language-ruby/

https://circleci.com/docs/2.0/caching/

https://discuss.circleci.com/t/caching-ruby-gems/21868/11

https://docs.gradle.org/current/userguide/build_cache.html

### TOOODOOOO TI 12.11

detox > CI

CI : image korvataan uusimmalla android SDK
gradlen päivitys uusimpaan

Detoxin asennus
Testien ajo
Kuvien otto detoxilla tiettyyn kansioon:
--artifacts-location kansion_polku

Kuvien kopiointi fastlane kansioon:
android/fastlane/metadata/android/fi-FI/images/phoneScreenshots -kansioon

Kopioi kuvat artifacts kansion alta fastlanen phoneScreenshots kansioon

find artifacts/android.emu.release*/*phoneScreenshots -name "\*.png" -exec cp {} android/fastlane/metadata/android/fi-FI/images/phoneScreenshots \;

android.emu.release = detox testi konfiguraation nimi (package.jsonissa)
\*phoneScreenshots = detox testitapauksen nimi (e2e kansiossa tiedostossa jossa testit määritellään)

NOTE TO SELF
git pull before any changes after CI

rm -rf node_modules && npm install

sdkmanager --list --verbose | grep system-images

Winning
https://support.circleci.com/hc/en-us/articles/360000028928-Testing-with-Android-emulator-on-CircleCI-2-0

Using google firebase as a test bench
https://firebase.google.com/docs/test-lab/android/command-line

CircleCI docker images
https://circleci.com/docs/2.0/docker-image-tags.json
https://circleci.com/docs/2.0/circleci-images/#nodejs

Android SDK versions
https://medium.com/androiddevelopers/picking-your-compilesdkversion-minsdkversion-targetsdkversion-a098a0341ebd

https://stackoverflow.com/questions/45424110/how-to-run-android-ui-tests-properly-on-circleci-2-0

Android App Bundle ABB
https://developer.android.com/guide/app-bundle/

---

## NOTES VKO 47

### Codepush / fastlane

patch/fix = codepush
minor = codepush madatory
major = google

install rnpm for npm

### TI 19.11

cz commitizen custom types

need to trigger codepush or google based on type of commit and whether it changes a version

https://github.com/commitizen/cz-cli

https://github.com/commitizen/cz-conventional-changelog

https://www.npmjs.com/package/cz-conventional-changelog

https://github.com/conventional-changelog/commitlint

npm cache clean

reset git

git reset --hard shanumber

### Uninstall java

https://java.com/en/download/help/mac_uninstall_java.xml

https://www.java.com/en/download/help/deployment_cache.xml

### fastlane

bundle exec fastlane beta release:true

Logic for fastfile / CI:
https://blog.echobind.com/automating-codepush-deploys-with-fastlane-51db8b5d5fc9

### codedpush stuff

Codepush

push notifications
saattaa tarvita firebasen...

AppCenter GoogleStore connection
Distribute > Store > add app json & com.app_name from android/app/build.gradle

### Done VKO 47

- Codepush connected to app
- App connected to app center
- Add changes to codepush and update app
- codepush installed on ci
- appcentrer login
- appcenter check connected apps
- upload to appcenter from ci
- access token is for login into app center

Is development key needed?

> YES, used to detect which version of the app the update is for

What is app secret is for?

> Dunno

Other ways to provide app secret json?

> Dunno yet

Codepush version label
https://stackoverflow.com/questions/39796511/how-do-i-show-the-current-codepush-version-label-in-my-react-native-android-app/39814193

---

## NOTES VKO 48

### VKO 48 Monday

codePush.notifyAppReady
https://stackoverflow.com/questions/52685589/codepush-updates-appversion-is-incorrect-its-always-the-same-as-the-current-a

Show current version and update info
https://stackoverflow.com/questions/39796511/how-do-i-show-the-current-codepush-version-label-in-my-react-native-android-app/39814193

---

Codepush manual labels
https://github.com/Microsoft/react-native-code-push/issues/1511

Versioning?
1.2.3.v3

1.2.3 = package.json
v3 = codepush internal versioning

---

codepush deployment history
https://docs.microsoft.com/en-us/appcenter/distribution/codepush/cli#installation

codepush react native integration with message upon update
http://blog.logicwind.com/codepush-integration-in-react-native-app/

Kyssäreitä

- onko putki tarkoitus pitää päällä vain master branchissa?
- tarvitaanko changelog.md muualla kuin masterissa?

https://stackoverflow.com/questions/14336411/how-to-get-previous-tag-name

https://gist.github.com/kjantzer/98dfa5138ad6e741c24f

git log
git log --grep='chore(release)' v1.4.0~1 -1 --pretty='%s'

get second to latest tag
git describe --abbrev=0 --tags \$(git rev-list --tags --skip=1 --max-count=1)

git second newest tag
git describe --abbrev=0 --tags v1.4.0^

git latest tag hash
git rev-list --tags --skip=1 --max-count=1

how to remove first char from string in ruby
UI.message previous_tag.tr('v','')
UI.message previous_tag[1..-1]

To remove fastlane plugins
https://docs.fastlane.tools/plugins/using-plugins/

### VKO 48 Tuesday

.env.xxx file?

renne:

- tehdäänkö me putkea appille, vai keskitytäänkö eri työkalujen "how to" sekä example appin dokumentointiin?
- jaakko will tell
- jaakolla on kiireitä
- renne paikalla ehkä huomenna
-

Detoxissa Android SDK version täytyy olla max 27, muuten tulee
DEBUG: [DetoxServer.js/CANNOT_FORWARD] role=testee not connected, cannot fw action

android sdk täytyy olla min 28
https://android-developers.googleblog.com/2019/02/expanding-target-api-level-requirements.html

Fastlane interacting with user
https://docs.fastlane.tools/advanced/actions/

update fastlane
sudo gem install fastlane

sync options etc
https://github.com/microsoft/react-native-code-push/blob/master/docs/api-js.md

semantic release toimii
codepush toimii
fastlane julkaisu toimii sekä codepushiin että googleen
logiikka ci putkessa
draw io kaavio tehty

- detox vain paikallisesti ja vain sdk 27 versiona
- android sdk täytyy olla 28 jos lataa googleen

### VKO 48 Wednesday

- Ei tehdä appille putkea
- dokumentointi sille tasolle että sen pohjalta saa idean siitä miten toimii

branchin pohjalta putki päälle
staging git push -> putki -> google internal & codepush staging
staging git branch merge to master -> putki -> master beta & codepush beta

- create alpha branch -> DONE
- alpha deployment key -> DONE
- googleen pitää tehdä alpha track julkaisu -> DONE
  - voiko tehdä cin kautta vai pitääkö eka ladata manuaalisesti -> VOI LADATA FASTLANELLA SUORAAN
- fastlane track promote with no build -> DONE
- codepush staging deployment päivitys -> DONE
- promote codepush update -> DONE
-
- staging merger -> korottaa googlen track -> beta, codepush deployment -> beta

to get app and codepush version:
const [metadata, setMetadata] = useState();

useEffect(() => {
// Get latest update info
const getMetadata = async () => {
const metadata = await codePush.getUpdateMetadata();
setMetadata(metadata);
};
getMetadata();
});

              {metadata && (
                <Text style={styles.sectionTitle}>
                  Current version: {metadata.appVersion}
                  {'\n'}
                  Current update: {metadata.label}
                  {'\n'}
                </Text>
              )}

npx appcenter login --token c1a4b725b705b53c5186f71ce9cde40d953ed876 && npx appcenter codepush release-react -a YOUR_USER/CiTestApp -d Alpha -t '^2.x.x'

### VKO 48 Thursday

appcenter ei julkaise päivityksiä jos applikaatiossa ei ole tapahtunut muutoksia

- package.json muutoksia ei tarkisteta

ERROR jos buildi epäonnistuu (lokaalisti) :

FAILURE: Build failed with an exception.

- What went wrong:
  Some problems were found with the configuration of task ':app:signingConfigWriterRelease'.
  > No value has been specified for property 'signingConfig.keyPassword'.
  > No value has been specified for property 'signingConfig.storePassword'.

tod näk johtuu siitä että ko. muuttujat eivät ole asetettu

How to run pipelines:
Idea:
Use DEV branches without a CI configuration
MERGE updates from DEV to QA branch once app seems to work well enough
use a pipeline for testing on QA
IF tests are ok, merge to ST

How to make builds/updates

- make changes to App
- git add .
- gi cz -> select feature/minor (x.1.x) or fix/patch (x.x.1), enter some message if change was breaking/major (1.x.x)
- git push
- -> starts pipeline -> creates updates, writes changelog, package.json on alpha branch and creates the tag

To promote updates:

- on alpha branch -> git pull to get the latest versions (important!)
- git checkout master
- git merge alpha
- git push
- > pushes changes done to alpha branches to master branch, including package.json (important)
- > promotes latest google/codepush updates

Notes:

- codepush deployment names are case-sensitive
- google track names are all small letters
  = kannattaa sopia yhteinen käytäntö miten branch,deployment ja track nimetaan -> ei tarvitse erikseen muistaa / koodata tsekkejä
- Codepush description kertoo päivityksen tyypin ja versiot
- versioiden pohjalta voi tarkistaa mm. gitin CHANGELOG.md tidostosta tai Releasien pohjalta mitkä muutokset oli tehty
- Jos descriptioon halutaan lisätä git viestejä, voi hyödyntää fastlane analyze commits -pluginia joka ymmärtää myös semver versiointia
- codepush päivityksen labelia ei voi muuttaa
- rivien vaihdot descriptionissa eivät välttämättä toimi oikein
- description viesti pitää laittaa lainausmerkkeihin, kuten myös codepush kohteen version arvo

### VKO 48 Friday

jostain syystä päivitykset eivät asennu

stringx.xml käytössä building aikana -> pitää olla eri deployment key jotta codepush päivitykset ohjautuvat oikein -> pitää tehdä erillinen buildi jos halutaan käyttää eri deploymentteja eri trackeille

https://github.com/npm/node-semver#advanced-range-syntax

1. Sama deployment key kaikissa trackeissa

   - Mahdollisuus testi päivityksen päätymiseen production versioon

2. Eri development key kaikissa trackeissa
   - Useampi buildi mutta codepush päivitykset ovat saatavilla vain ko. trackiin

Vaihtoehto 2 ei mahdollista ei julkisten päivitysten ja buildien päätymisen productioniin mutta vaatii erillisen buildin tekemisen jokaisen major version kohdalla

Vaihtoehto 1 ei vaadi erillistä buildia eli googlen buildit voidaan promota, mutta codepush päivitysten kohde (semver) täytyy määrittää käyttäen -beta tms semver tagia https://github.com/npm/node-semver#advanced-range-syntax jotta testi päivitykset kohdistuvat vain tiettyyn versioon

--> draw.io päivitys

Tarkkana versiopäivitysten kanssa
päivitykset masteriin vain mergellä

## VKO 49

### VKO 49 Monday

VKO 49
Maanantai

googlen apk version code pitää olla eri kaikissa trackeissa, ongelmaa ei tule jos trackit promotetaan alpha-beta-jne, mutta jos ladataan uusia buildeja (jos pitää käyttää eri codepush deployment avaimia...) niin versiocode pitää olla suurempi kuin alphassa. Ehkä olisi hyvä käyttää jotain muuta kuin semveriä version codena? Esim googlen antamaa suositusta jossa versioncodessa mainitaan APK, näyttöjen koot, varsinainen version nro
https://developer.android.com/google/play/publishing/multiple-apks.html#VersionCodes

Ehkä mahdollisat konffata deployment key tähään tyyliin:
https://github.com/microsoft/react-native-code-push/blob/master/docs/multi-deployment-testing-android.md

version_code X 100000
viimeinen numero 0-3 = track

Codepushin sovittaminen yhteen Google Play storen kanssa vaatii hieman säätöä jotta Google appien trackit ja codepushin deploymentit eivät mene sekaisin.
