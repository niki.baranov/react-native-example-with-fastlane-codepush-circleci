# Awesome project (React Native example app)

To create a React Native example project you will first need to install react native and react native cli to your system. To be able to do that, you will also need to install Java JDK as well as NodeJS and most likely Watchman.

https://www.java.com/en/

https://nodejs.org/en/ (with NPM)

https://facebook.github.io/watchman/

https://facebook.github.io/react-native/

## How to list for the project

1. Setup React Native example project [Awesome project](1-awesome-project.md)
   - Example project setup
2. Install Commitizen [Commitizen](2-commitizen.md)
   - To ensure that all commits are standardized
3. Install Semantic Release [Semantic release](3-semantic-release.md)
   - To create CHANGELOG, tag each release with new version and update package.json version
4. Setup CI [CircleCI](4-ci.md)
   - Configuration of CI to work with semantic-release and other tools
5. Integrate Fastlane [Fastlane](5-fastlane.md)
   - To automate publishing to Google
6. Integrate Codepush [Codepush](6-codepush.md)
   - To automate small updates
7. Integrate Detox [Detox](7-detox.md)
   - To automate tests & screenshot generation
8. Good to know [Good To Know](8-good-to-know.md)
   - Various things that are good to keep in mind

## Setting up react native example project

To install react-native simply run this:
`sudo npm i react-native-cli` if you prefer installing it globally, then: `sudo npm i -g react-native-cli`

To uninstall it for some reason: `sudo npm uninstall -g react-native-cli`

Running the command below will create a full react native example project with all the necessary dependencies.

`npx react-native init PROJECT_NAME`

This will likely ask for cocoapods (at least on mac) and provide options gem or homebrew. If you are using gem install, you will also need to have ruby installed. <https://www.ruby-lang.org/en/>

`sudo gem install cocoapods`

You will also likey need to install xCode, which is iOS specific development tool as well as watchman. XCode is available from iOS appstore and it is free on MAC

`brew install watchman`

After installation go to project root folder and execute `npm run android` `or npm run ios` , will initialize the emulator (make sure you have at least one emulator created).

## AVD and SDK managers

There are couple of ways to setup an android device emulator, one is via the UI of android studio and the other is manually via android studio cli. At some point you will also need to accept android licenses.

If you are using the UI, creation is super simple;

1. Start Android studio
2. Click `configure` >> AVD Manager (This will open a separate window)
3. Click on the `+ Create Virtual Device`
4. Select the type of device you want to emulate (e.g. Phone >> Nexus 5) Click `Next`
5. Select the System Image, We'll go with Q (API 29)
6. Give your emulator device a name, default will consist of the device type and API version
7. Click `Finish` >> Done

To setup an emulator using cli, you will first need to download the system image using skdmanager. To see which images are available you can run:

`sdkmanager --list --verbose | grep system-images`.

To download the image simply run: `sdkmanager "full_image_name"`

For e.g. `sdkmanager "system-images;android-29;google_apis;x86"` would install an image for android API 29 x86 architecture. Keep in mind, that running other than x86 emulator on a "normal" computer might not work at all or can be extremely slow.

Once the image is downloaded you can setup actual emulator with `avdmanager`.

Fro e.g. running this: `avdmanager --verbose create avd -f -n Nexus_5X_test2 -d "Nexus 5X" -k "system-images;android-29;google_apis;x86"` would create an emulator for Nexus 5X device with Nexus_5X_test name using the system image we just downloaded. The `--verbose` command will provide extra details that can help with debugging.

You can pass all kinds of options to avdmanager when creating the emulated device, for all possible options refer to the Android Emulator documentation: <https://developer.android.com/studio/run/emulator>

To check what AVDs are currently setup on your system run:

`avdmanager list avd`

To start the emulator manually from cli, you will first need to have SDK emulator installed:

`sdkmanager --install emulator`

To run your emulator use:

`emulator @Nexus_5X_test2`

For more info on sdkmanager refer to <https://developer.android.com/studio/command-line/sdkmanager>

## Sources

[React Native Getting started (Facebook @GitHub)](https://facebook.github.io/react-native/docs/getting-started)

[React Native interactable (WIX @GitHub)](https://github.com/wix/react-native-interactable/issues/228)
