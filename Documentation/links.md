# Links

Here you will find links to the tools we used as well as other tools we found along the way and many interesting articles regarding the creation of fully automated CICD-pipeline for a mobile application.

- [Definitions](#definitions)
- [Commitizen](#commitizen)
- [Semantic-release](#semantic-release)
- [Commitizen](#commitizen)
- [NPM Plugins](#npm-plugins)
- [Fastlane](#fastlane)
- [Fastlane and CircleCI for Android](#fastlane-and-circleci-for-android)
- [Fastlane for Android](#fastlane-for-android)
- [CircleCi](#circleci)
- [Android](#android)
- [Android with CircleCi](#android-with-circleci)
- [Android Ci with Fastlane and CircleCi](#android-ci-with-fastlane-and-circleci)
- [Codepush](#codepush)
- [React Native](#react-native)
- [React Native with CodePush](#react-native-with-codepush)
- [Firebase](#firebase)
- [Android SDK](#android-sdk)
- [Other Articles](#other-articles)

## Definitions

[SemVer specification](https://semver.org/)

[Conventional Commits specification](https://www.conventionalcommits.org/en/v1.0.0/)

[RFC2119 specification](https://tools.ietf.org/html/rfc2119)

## Commitizen

[Commitizen adapters (commitizen @GitHub)](https://github.com/commitizen/cz-cli#adapters)

[Commitizen plugin for npm (commitizen @GitHub)](https://github.com/commitizen/cz-cli)

[Git-cz plugin for npm (npmjs)](https://www.npmjs.com/package/git-cz)

[Commitlint plugin for npm (conventional-changelog @GitHub)](https://github.com/conventional-changelog/commitlint)

[Conventional changelog plugin for npm (conventional-changelog @GitHub)](https://github.com/conventional-changelog/conventional-changelog)

[Conventional commits detector plugin for npm (conventional-changelog @GitHub)](https://github.com/conventional-changelog/conventional-commits-detector)

## Semantic Release

[Semantic Release](https://semantic-release.gitbook.io/semantic-release)

[Semantic Release plugin for npm (semantic-release @GitHub)](https://github.com/semantic-release/semantic-release)

[Semantic Release Docs](https://github.com/semantic-release/semantic-release/blob/master/docs/usage/plugins.md)

## Fastlane

[Fastlane Docs (Fastlane)](https://docs.fastlane.tools/)

[Fastlane Docs getting started (Fastlane)](https://docs.fastlane.tools/getting-started/android/setup/)

[Fastlane changelog_from_git_commits (Fastlane)](https://docs.fastlane.tools/actions/changelog_from_git_commits/)

[Release notes using semantic release for fastlane (Blueapron.io)](https://blog.blueapron.io/release-notes-using-fastlane-fba80240dc4d)

[Fastlane load json plugin (KrauseFx @GitHub)](https://github.com/KrauseFx/fastlane-plugin-load_json)

[Semantic release plugin for Fastlane (xotahal @GitHub)](https://github.com/xotahal/fastlane-plugin-semantic_release)

[Semantic Release for Fastlane (Medium Blog)](https://blog.usejournal.com/semantic-release-for-fastlane-781df4cf5888)

### Fastlane and CircleCI for Android

[Fastlane + CircleCI for Android — Part 1 (Medium)](https://medium.com/open-knowledge/fastlane-circleci-for-android-part-1-471e29b968b2)

[Fastlane + CircleCI for Android — Part 2 (Medium)](https://medium.com/open-knowledge/fastlane-circleci-for-android-part-2-3dedadac89c2)

[Fastlane + CircleCI for Android — Part 3 (Medium)](https://medium.com/open-knowledge/fastlane-circleci-for-android-part-3-f2282bc30571)

### Fastlane for Android

[Fastlane for Android — Automate Everything (Part 1) (Medium)](https://medium.com/open-knowledge/fastlane-for-android-automate-everything-part-i-ad42bbb15365)

[Fastlane for Android — Automate Everything (Part 2) (Medium)](https://medium.com/open-knowledge/fastlane-for-android-automate-everything-part-2-bb200076a697)

[Fastlane for Android — Automate Everything (Part 3) (Medium)](https://medium.com/open-knowledge/fastlane-for-android-automate-everything-part-3-9f1973600c01)

## CircleCi

[CircleCi nodejs images (CircleCi)](https://circleci.com/docs/2.0/circleci-images/#nodejs)

[Docker images tags (CircleCi)](https://circleci.com/docs/2.0/docker-image-tags.json)

[Language Guide: Android (CircleCi)](https://circleci.com/docs/2.0/language-android/)

[Testing with Android emulator on CircleCI 2.0 (CircleCi)](https://support.circleci.com/hc/en-us/articles/360000028928-Testing-with-Android-emulator-on-CircleCI-2-0)

[Emulator not launching… forever (CircleCi)](https://discuss.circleci.com/t/emulator-not-launching-forever/15892/2)

[facebookincubator Ci (CircleCi)](https://circleci.com/gh/facebookincubator/spectrum/294)

## Android

[Android App Bundle (Android)](https://developer.android.com/guide/app-bundle/)

[Picking your compileSdkVersion, minSdkVersion, and targetSdkVersion (Medium)](https://medium.com/androiddevelopers/picking-your-compilesdkversion-minsdkversion-targetsdkversion-a098a0341ebd)

[Install and Create Emulators using AVDMANAGER and SDKMANAGER (mrk-han @GitHub)](https://gist.github.com/mrk-han/66ac1a724456cadf1c93f4218c6060ae)

## Android with CircleCi

[Inventory Agent for Android (flyve-mdm @GitHub)](https://github.com/flyve-mdm/android-inventory-agent)

[How to run Android UI tests properly on CircleCI 2.0? (StackOverflow)](https://stackoverflow.com/questions/45424110/how-to-run-android-ui-tests-properly-on-circleci-2-0)

[Run Android emulator on CircleCI 2.0 macOS (DoguD @Github)](https://gist.github.com/DoguD/58b4b86a5d892130af84074078581b87)

[CircleCI with Android (proandroiddev)](https://proandroiddev.com/circleci-with-android-continuous-integration-3ecd98f92bd4)

## Android Ci with Fastlane and CircleCi

[Android Continuous Integration using Fastlane and CircleCI 2.0 — Part I (Medium)](https://medium.com/pink-room-club/android-continuous-integration-using-fastlane-and-circleci-2-0-part-i-7204e2e7b8b)

[Android Continuous Integration using Fastlane and CircleCI 2.0 — Part 2 (Medium)](https://medium.com/pink-room-club/android-continuous-integration-using-fastlane-and-circleci-2-0-part-ii-7f8dd7265659)

## Codepush

[Microsoft App Center CLI (Microsoft @GitHub)](https://github.com/microsoft/appcenter-cli)

[App Center Push (Microsoft Docs)](https://docs.microsoft.com/en-us/appcenter/push/)

[CodePush Management CLI (Microsoft @GitHub)](https://github.com/microsoft/code-push/tree/master/cli)

[React Native Module for CodePush (Microsoft @GitHub)](https://github.com/microsoft/react-native-code-push#getting-started)

[Build the React Native Deployment Pipeline (Thedo)](https://blog.theodo.com/2019/04/react-native-deployment-pipeline/)

## React Native

[React Native Getting started (Facebook @GitHub)](https://facebook.github.io/react-native/docs/getting-started)

[React Native interactable (WIX @GitHub)](https://github.com/wix/react-native-interactable/issues/228)

## React Native with CodePush

[React Native Code push (Microsoft GitHub)](https://github.com/microsoft/react-native-code-push)

[React Native Code push API (Microsoft GitHub)](https://github.com/microsoft/react-native-code-push/blob/master/docs/api-js.md)

[React Native code push (Microsoft GitHub)](https://github.com/microsoft/react-native-code-push/blob/master/Examples/create-app.js)

[React Native code push examples (Microsoft GitHub)](https://github.com/microsoft/react-native-code-push/tree/master/Examples/CodePushDemoApp/android/app)

[React Native CodePush (Medium)](https://medium.com/spritle-software/react-native-codepush-b86f0ea8432c)

[Get started with ‘CodePush’ (React-Native) (Medium)](https://medium.com/@rajanmaharjan/get-started-with-wonderful-technology-d838aafdc2d3)

[The right way to code push react native (Medium)](https://medium.com/fundbox-engineering/the-right-way-to-code-push-react-native-cba82d0f8ec9)

[React Native and Code Push example (Tyler Buchea Blog)](https://blog.tylerbuchea.com/react-native-and-code-push-example/)

[App Center react-native (Microsoft Docs)](https://docs.microsoft.com/en-us/appcenter/distribution/codepush/react-native)

[App Center codepush (Microsoft Docs)](https://docs.microsoft.com/en-us/appcenter/distribution/codepush/)

[React Native plugin - getting started (Microsoft Docs)](https://docs.microsoft.com/en-us/appcenter/distribution/codepush/react-native#getting-started)

[React Native plugin- android setup (Microsoft Docs)](https://docs.microsoft.com/en-us/appcenter/distribution/codepush/react-native#android-setup)

[React Native plugin - plugin useage(Microsoft Docs)](https://docs.microsoft.com/en-us/appcenter/distribution/codepush/react-native#plugin-usage)

[ (Microsoft GitHub)](https://github.com/Microsoft/code-push/tree/master/cli#access-keys)

[ (Microsoft GitHub)](https://github.com/microsoft/react-native-code-push/blob/master/docs/api-js.md#codepushsync)

[Codepush Integration in react native (LogicWind)](http://blog.logicwind.com/codepush-integration-in-react-native-app/)

## Firebase

[Firebase Test Lab (Google)](https://firebase.google.com/docs/test-lab)

[Get started with Firebase Test Lab from the gcloud Command Line (Google)](https://firebase.google.com/docs/test-lab/android/command-line)

[All you need to know about CircleCI 2.0 with Firebase Test Lab](https://medium.com/@ayltai/all-you-need-to-know-about-circleci-2-0-with-firebase-test-lab-2a66785ff3c2)

## Android SDK

[Picking your SDK versions (Medium)](https://medium.com/androiddevelopers/picking-your-compilesdkversion-minsdkversion-targetsdkversion-a098a0341ebd)

[Distributing beta builds (thecodingmachine @GitHub)](https://github.com/thecodingmachine/react-native-boilerplate/blob/master/docs/beta%20builds.md)

[Using Gitlab CI/CD + Fastlane for iOS Project — Part 1 (Medium)](https://medium.com/@phanquanghoang/using-gitlab-ci-cd-fastlane-for-ios-project-part-1-5e7db82a3566)

[Using Gitlab CI/CD + Fastlane for iOS Project — Part 2 (Medium)](https://medium.com/@phanquanghoang/https-medium-com-phanquanghoang-using-gitlab-ci-cd-fastlane-for-ios-project-part-2-f2c55bf6305e)

[Using Gitlab CI/CD + Fastlane for iOS Project — Part 3 (Medium)](https://medium.com/@phanquanghoang/using-gitlab-ci-cd-fastlane-for-ios-project-part-3-f710b618da4a)

[Android APP bundle (Android)](https://developer.android.com/guide/app-bundle/)

## Detox

[Detox introduction for Android](https://github.com/wix/Detox/blob/master/docs/Introduction.Android.md)

[Detox screenshot (Wix GitHub)](https://github.com/wix/Detox/tree/master/docs)

[v27 vs v28 [DetoxServer.js/CANNOT_FORWARD] role=testee not connected -error](https://github.com/wix/Detox/issues/1716)

[Google Play Store screenshot guide](https://thetool.io/2019/app-store-google-play-screenshot-sizes-guidelines)

## Other Articles

[Adding Continuous Integration to your Android project (lordcodes)](https://www.lordcodes.com/posts/adding-continuous-integration-to-your-android-project)

[Component tns-core-modules & tns-android & tns-ios is not installed #5690 (NativeScript @Github)](https://github.com/NativeScript/NativeScript/issues/5690#issuecomment-381722271)

[The way to fully automated releases in open source projects (Medium)](https://medium.com/@kevinkreuzer/the-way-to-fully-automated-releases-in-open-source-projects-44c015f38fd6)

[React Native Getting Started guide](https://facebook.github.io/react-native/docs/getting-started)

[Node.js](https://nodejs.org/en/)

[Making your node.js work everythwere with environment variables (Medium)](https://medium.com/the-node-js-collection/making-your-node-js-work-everywhere-with-environment-variables-2da8cdf6e786)

[Shipping React Native apps with Fastlane (Carloscuesta.me)](https://carloscuesta.me/blog/shipping-react-native-apps-with-fastlane/)

[React Native DevOps Guide (Medium)](https://medium.com/@tgpski/react-native-devops-guide-2d8e4755ebee)

[How to automate your releases, versioning & release notes like a boss (Medium)](https://medium.com/faun/automate-your-releases-versioning-and-release-notes-with-semantic-release-d5575b73d986)

[Automate React Native Deployment - Part 1 (timmywil.com blog)](https://timmywil.com/blog/Automate-React-Native-Deployment-Part-1/)

[Automate React Native Deployment - Part 2 (timmywil.com blog)](https://timmywil.com/blog/Automate-React-Native-Deployment-Part-2/)

[Under The Hood: Ramping up for launch — CI/CD with React Native, Gitlab, and Fastlane (Medium)](https://medium.com/ferly/under-the-hood-ramping-up-for-launch-ci-cd-with-react-native-gitlab-and-fastlane-e2df6fa43fd1)

[Gradle](https://gradle.org/install/)

[Bundler](https://bundler.io/v1.10/man/bundle-install.1.html)

[Husky plugin for npm](https://github.com/typicode/husky)
