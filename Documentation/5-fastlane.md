# Fastlane

Configuring Fastlane to work with CircleCI does not really need anything special. Our config.yml installs all required NPM and gem dependencies using a `circleci/android:api-28-node8-alpha` image and after executing `npx semantic-release` we run fastlane lanes based on the branch.

## Install fastlane

To install Fastlane, follow the instructions here:
<https://docs.fastlane.tools/getting-started/android/setup/>

## Gradle and Google goodies

For gradle to be able to upload and build the app, several vars need to be setup, this can be done by setting them up as env vars in the CI (Gitlab or CirlceCI). I might also be a good idea to encode variables for additional safety.

You will likely need to do all of the following:

- Create a keystore file and passwords
- Define keystore passwords in build.gradle (within android directory of your app)
- Set environment variables for keystore passwords
- Define keystore file and keystore file alias in gradle.properties
- Set keystore file as base64 encrypted environment variable
- Create CircleCI script to decrypt and create keystore file upon build
- Set google API key as environment variable
- Initiate build via Fastlane based on git branch

### Keystore file & passwords

Keystore file is needed by Google Play Store to authenticate that the build is indeed yours, dont lose the keystorefile you create and use upon the first release to Google Play Store as it will check and compare it upon every release. You can generate keystorefile bu running the following command:

```
keytool -genkeypair -v -keystore my-key.keystore -alias my-key-alias -keyalg RSA -keysize 2048 -validity 10000
```

You can change `my-key`.keystore and `my-key-alias` parts to anything you like (just dont forget to update the names in the scripts...)

Next, define my-key.keystore and my-key-alias in gradle.properties, you will later use the variables you define here in build.gradle file

```
RELEASE_STORE_FILE=my-key.keystore
RELEASE_KEY_ALIAS=my-key-alias
```

Once you have created the keystore file for your google app, create the environment variables within your CI to store the passwords you used for the key, make note of the variable names you used as you will add them into build.gradle (within the android/app directory)

Define these passwords at the top of your build.gradle (or at least before your script uses them...)

```
def keypass = System.getenv("STORE_PWD")
def keyalias = System.getenv("KEY_PWD")
```

The commands above pull the values of `STORE_PWD` and `KEY_PWD` env variables (let's assume you used those variable names). Later in the android/app/build.gradle you should have something like this (inside android configuration):

```
    signingConfigs {
        debug {
            storeFile file(RELEASE_STORE_FILE)
            storePassword keypass
            keyAlias RELEASE_KEY_ALIAS
            keyPassword keyalias
        }
    }
```

Here you refer to the variables you set for keystore files and alias you created earlier as well as to the variables you set for the passwords. Once these are set, your gradle configuration is pretty much done.

### Encrypt keystore file

You can generate a base64 encrypted string of your keystore file by running the following command in shell (in the folder with your keystore file):

`base64 your_keystore_file`

This will print out an encoded string that includes the content of your keystorefile, you can then copy-paste it into CI as environment variable.

### CircleCI decrypt script & recreate keystore file

When gradle tries to build your app, it will look for the keystore file from a predefined location (in our case this location was android/app). Since we don't want to keep this rather important file in our repository but do want to make thing as automated as possible, we need to somehow recreate the file through CI before fastlane initiates the build. To do this we can create a tiny script within CI before fastlane call.

For CircleCI

```
  decode_key: &decode_key
    run:
      name: Decode Android key store
      command: echo $KEYSTOREFILE | base64 -d > android/app/my-key.keystore
```

For GitLabCI

```
  script:
    - echo $KEYSTOREFILE | base64 -d > android/app/my-key.keystore
```

You can add this script in the `references` section of your circleci config.yml and then call it before you initiate fastlane. The command above looks for a `KEYSTOREFILE` environment variable and echoes it via base64 decoder into a file within the location we specify. Keep in mind, this file will only exist within the container used to build and depoloy the app, once the job is done, or once it fails, the container and everything in it will be destroyed.

### Google API

Before you can upload anything to Google Play Store, you will also need to generate a Google API JSON file and once again, from the CI point of view your best choice is to add the content of the generated JSON into a environment variable within your CI.

You would then refer to it from Fastlane `Appfile` like this (replace `GOOGLE_APP_JSON_KEY` to anything you like):

```
json_key_data_raw(ENV['GOOGLE_APP_JSON_KEY'])
```

Appfile is used only by Fastlane so if you use some other way to release your app, you might need to use the JSON in some other way, having it in your CI however, will simplify things a lot and is way more secure than having an actual file within your repo.

### Initiating builds from fastlane

Practically all configuration for fastlane is within Fastfile (you can also use an Appfile to define some things for fastlane). Our fastfile verifies and checks few things before actually initiating gradle to build the app, all these small checks are run through private_lanes to keep them out of reach from direct access, eg:

```
  private_lane :check_json_version do
    content = load_json(json_path: "./../package.json")
    version = content["version"]
  end
```

The above uses fastlane `load_json` plugin to load the content of the package.json file and defines the version as based on the value of version within the json. The same can be accomplished with a shell command.

```
  private_lane :version_to_array do |options|
    version = options[:version]
    version_split = version.split(".")

    if version_split.length != 3
      UI.user_error! "Can't parse version #{version}"
    end
    version_split
  end
```

Thw version code you will submit to google play store, NEEDS to be higher than the previous. You can use fastlanes `google_play_track_version_codes(package_name: package_base_name, track: track).max` to find out the highest used version code on a specific track Where package_name is the name of the app package (likely something like com.yourappname) and track is the track in Google Play Store (alpha, beta, release etc.)

NOTE! Every build **uploaded** to Google Play store has to have a different version code, however, if a build is promoted from eg. Alpha to Beta, the same version code used in Alpha will be also used for Beta, in addidion, when a build is promoted the "lower" track will be disabled by default.

### Creating and uploading new builds

After all the checks we initiate the build initiating `build_release` private_lane that builds the app and uploads it to Google Play Store. You can change between APK and AAB builds by changing the task to either bundle = aab or assemble = apk. Its also a good idea to clean gradle before creating a new build. A --build-cache flag is also used to cache some of the gradle stuff, keep in mind that not all of gradle will be saved in cache, this is by design.

```
  private_lane :build_release do |options|

  # get build name and build number
  version = options[:version]
  version_code = options[:version_code]
  release = options [:release]

  # Create new build
  # Create AAB
  # Use Cache
  gradle(task: 'clean', project_dir: 'android/')  # clean gradlew before creating new build
  gradle(
          task: 'bundle',
          build_type: release,
          project_dir: 'android/',
          flags: '--build-cache',
          properties: {
              'vcode' => version_code,
              'vname' => version
          }
      )
  end
```

### Version name and version code

The `vcode` and `vname` properties are defined in the android/app/build.gradle file so you also need to add the code below to build.gradle.

```
        versionCode project.hasProperty("vcode") ? project.property("vcode") as Integer : SET A DEFAULT VALUE TO USE
        versionName project.hasProperty("vname") ? project.property("vname") : "SET A DEFAULT VALUE TO USE"
```

### Where to place screenshots

Copy images to `android/fastlane/metadata/android/fi-FI/images/phoneScreenshots` -directory.

To find screenshots created with Detox and copy them to correct metadata folder:

`find artifacts/android.emu.release*/*TEST_NAME -name "\*.png" -exec cp {} android/fastlane/metadata/android/fi-FI/images/phoneScreenshots \;`

> `android.emu.release` = Detox test configuration (package.jsonissa)
>
> `\*TEST_NAME` = Detox test name (inside e2e directory where you define the tests). The \* is needed because detox creates date related information infront of the test name.

### ENV

ENV variables for localisation might need to be setup this should be done in CI if fastlane is executed there.

`LC_ALL=en_US.UTF-8`

`LANG=en_US.UTF-8`

## Tips, errors and fixes

Error message during aab build:
**apkNotificationMessageKeyBundleTargetsInvalidLanguage: The bundle targets unrecognized languages: fb**

FIX by adding:

```
android {
   defaultConfig {
        ...
        resConfigs "en", "US"
        ...
   }
}
```

Update Fastlane: `sudo gem install fastlane`

Update bundle file to use the latest fastlane: `bundle install`

This Fastlane plugin helps picking up specific info from within package.json file, for example version.
<https://github.com/KrauseFx/fastlane-plugin-load_json>

You can use fastlane by having a Fastlane directory inside the project directory or having one inside android or ios directories.

If you dont want to use a npm version semantic-release, you can use a fastlane plugin to do the same, kinda: <https://github.com/xotahal/fastlane-plugin-semantic_release>.

## Full Fastlane-file

```
default_platform(:android)

platform :android do

  # build a new app build and uploads to google
  private_lane :build_release do |options|

    # get build name and build number
    version = options[:version]
    version_code = options[:version_code]
    release = options [:release]

    # Create new build
    # Create AAB
    # Use Cache
    gradle(task: 'clean', project_dir: 'android/')  # clean gradlew before creating new build
    gradle(
            task: 'bundle',
            build_type: release,
            project_dir: 'android/',
            flags: '--build-cache',
            properties: {
                'vcode' => version_code,
                'vname' => version
            }
        )
    end
  # end of build_release

  # checks the value of the second to last tag
  private_lane :check_previous_tag do |options|
    version = options[:version]
    previous_tag = "git describe --abbrev=0 --tags v#{version}^"
    sh(previous_tag) do |status, result, command|
      unless status.success?
        UI.error "Command #{command} failed with status #{status.exitstatus}."
      end
      result
    end
  end
  # end of check_previous_tag

  # transform version string to array for 1:1 check
  private_lane :version_to_array do |options|
    version = options[:version]
    version_split = version.split(".")

    if version_split.length != 3
      UI.user_error! "Can't parse version #{version}"
    end
    version_split
  end
  # end of version_to_array

  # Check some value based on passed key from package.json
  private_lane :check_json do |options|
    key = options[:key]
    json = load_json(json_path: "package.json")
    value = json[key]
    UI.message "Key : value combo: #{key} : #{value}"
    value
  end
  # end of check_json

  # check what update to run
  lane :update do |options|

    # define vars
    version = check_json(key: "version")  # get version from json
    track = options[:track]               # set track
    codepush_app = "YOUR_USER/CiTestApp"   # app user/name
    case options[:release]                # check & set release type
      when 'pro'
        release = 'Release'
      when 'st'
        release = 'Staging'
      when 'dev'
        release = 'Development'
      else
        UI.user_error! "Release #{options[:release]} is not allowed!"
    end

    # get the previously used tag value as integer
    previous_version = check_previous_tag(version: version)[1..-1]
    previous_version_arr = version_to_array(version: previous_version)
    current_version_arr = version_to_array(version: version)

    # set google play store version code / build number
    version_code = Integer(current_version_arr[0]) * 100000 + Integer(current_version_arr[1]) * 1000 + Integer(current_version_arr[2]) * 10

    UI.message "Current version: #{version}"
    UI.message "Current version code: #{version_code}"
    UI.message "Previous version: #{previous_version}"
    UI.message "Release set to: #{release}"
    UI.message "Track set to: #{track}"

    # version is minor aka feat >> upload to codepush with --mandatory tag
    if current_version_arr[1].to_i > previous_version_arr[1].to_i
        UI.message "This is a feat/minor update"
        description = "This is a MINOR update for version #{previous_version} --> #{version}"
        CODEPUSH_CMD = "npx appcenter login --token #{ENV['APPCENTERTOKEN']} && npx appcenter codepush release-react -a #{codepush_app} -d #{track.capitalize} -t '^#{current_version_arr[0]}.x.x' -m --description '#{description}'"
        Dir.chdir("..") do
          sh(CODEPUSH_CMD) do |status, result, command|
            unless status.success?
              UI.error "Command #{command} failed with status #{status.exitstatus}."
            end
            result
          end
        end
    else
    # version is patch aka fix >> upload to codepush without --mandatory
      if current_version_arr[2].to_i > previous_version_arr[2].to_i
        UI.message "This is a fix/patch"
        description = "This is a FIX update for version #{previous_version} --> #{version}"
        CODEPUSH_CMD = "npx appcenter login --token #{ENV['APPCENTERTOKEN']} && npx appcenter codepush release-react -a #{codepush_app} -d #{track.capitalize} -t '^#{current_version_arr[0]}.x.x' --description '#{description}'"
        Dir.chdir("..") do
          sh(CODEPUSH_CMD) do |status, result, command|
            unless status.success?
              UI.error "Command #{command} failed with status #{status.exitstatus}."
            end
            result
          end
        end
      else
      # If version is major aka build >> upload to google
        if ((version_code / 100000) > (google_play_track_version_codes(package_name: check_json(key: "name"), track: track).max / 100000))
          UI.message "This is a new build/major update"

          # upload new build to google
          build_release(version: version, version_code: version_code, release: release)

          # upload to google
          upload_to_play_store(track: track)

        else # IF no updates are necessary aka ci, chore etc...
          UI.message "No update necessary"

        end
        # end of loop
      end
      # end of loop
    end
    # end of loop
  end
  # end of check_update lane

  # Promote update or create new buld
  lane :promote do |options|

    version = check_json(key: "version")  # get version from json
    track_to_promote = options[:track_to_promote]
    track_to_promote_to = options[:track_to_promote_to]
    current_version_arr = version_to_array(version: version)
    codepush_app = "YOUR_USER/CiTestApp"   # app user/name

    # set google play store version code / build number
    UI.message "Increasing version code by +1 to avoid having same version as in alpha"
    version_code = Integer(current_version_arr[0]) * 100000 + Integer(current_version_arr[1]) * 1000 + Integer(current_version_arr[2]) * 10

    UI.message "Current version: #{version}"
    UI.message "Current version code: #{version_code}"
    UI.message "Track to promote: #{track_to_promote}"
    UI.message "Track to promote to: #{track_to_promote_to}"

    # If update is major aka build >> promote latest update
    # check what is the latest build version on updatable track
    if ((version_code / 100000) > (google_play_track_version_codes(package_name: check_json(key: "name"), track: track_to_promote_to).max / 100000))

      #UI.message "Promote google track #{track_to_promote} to #{track_to_promote_to}"
      UI.message "Cleaning gradle before new build"
      gradle(task: 'clean', project_dir: 'android/')  # clean gradlew before creating new build'

      # upload new build to google
      # version_code last number represents google track = 0 internal, 3 production
      case options[:track_to_promote_to]                # check & set release type
        when 'alpha'
          version_code = version_code + 1
        when 'beta'
          version_code = version_code + 2
        when 'production'
          version_code = version_code + 3
      end
      UI.message "Creating a new build: #{version} with version code: #{version_code} for track: #{track_to_promote_to}"
      build_release(version: version, version_code: version_code, release: "Release")

      # upload to google
      UI.message "Uploading a new build to track #{track_to_promote_to}"
      upload_to_play_store(track: track_to_promote_to)

    else
    # update is not major, promote latest codepush update (will keep mandatory status set to the update)
      UI.message "Promote codePush update #{track_to_promote} to #{track_to_promote_to}"

      CODEPUSH_CMD = "npx appcenter login --token #{ENV['APPCENTERTOKEN']} && npx appcenter codepush promote -a #{codepush_app}  -s #{track_to_promote.capitalize} -d #{track_to_promote_to.capitalize} -t '^#{current_version_arr[0]}.x.x'"
      Dir.chdir("..") do
        sh(CODEPUSH_CMD) do |status, result, command|
          unless status.success?
            UI.error "Command #{command} failed with status #{status.exitstatus}."
          end
          result
        end
      end
    end
    # end of loop
  end
  # end of promote lane
end
# end of android
```

## Sources

[Fastlane Docs (Fastlane)](https://docs.fastlane.tools/)

[Fastlane Docs getting started (Fastlane)](https://docs.fastlane.tools/getting-started/android/setup/)

[Fastlane changelog_from_git_commits (Fastlane)](https://docs.fastlane.tools/actions/changelog_from_git_commits/)

[Release notes using semantic release for fastlane (Blueapron.io)](https://blog.blueapron.io/release-notes-using-fastlane-fba80240dc4d)

[Fastlane load json plugin (KrauseFx @GitHub)](https://github.com/KrauseFx/fastlane-plugin-load_json)

[Semantic release plugin for Fastlane (xotahal @GitHub)](https://github.com/xotahal/fastlane-plugin-semantic_release)

[Semantic Release for Fastlane (Medium Blog)](https://blog.usejournal.com/semantic-release-for-fastlane-781df4cf5888)

### Fastlane and CircleCI for Android

[Fastlane + CircleCI for Android — Part 1 (Medium)](https://medium.com/open-knowledge/fastlane-circleci-for-android-part-1-471e29b968b2)

[Fastlane + CircleCI for Android — Part 2 (Medium)](https://medium.com/open-knowledge/fastlane-circleci-for-android-part-2-3dedadac89c2)

[Fastlane + CircleCI for Android — Part 3 (Medium)](https://medium.com/open-knowledge/fastlane-circleci-for-android-part-3-f2282bc30571)

### Fastlane for Android

[Fastlane for Android — Automate Everything (Part 1) (Medium)](https://medium.com/open-knowledge/fastlane-for-android-automate-everything-part-i-ad42bbb15365)

[Fastlane for Android — Automate Everything (Part 2) (Medium)](https://medium.com/open-knowledge/fastlane-for-android-automate-everything-part-2-bb200076a697)

[Fastlane for Android — Automate Everything (Part 3) (Medium)](https://medium.com/open-knowledge/fastlane-for-android-automate-everything-part-3-9f1973600c01)
