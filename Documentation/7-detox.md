# Detox

## How to install detox

npm install -g detox-cli
npm install detox --save-dev
npm install mocha --save-dev
detox init -r mocha

## How to configure detox

IF fb sdk init errors:

Add this to android/app/src/main/res/values/strings.xml:

```
<string name="facebook_app_id">CiTestApp</string>

android/app/src/main/AndroidManifest.xml inside <application> tag
<meta-data android:name="com.facebook.sdk.ApplicationId"
          android:value="@string/facebook_app_id"/>
```

## How to run detox with Ci

Build the apk that will be tested against before running the tests
use --l verbose after the detox command to get more info

## Useful commands & tips

Reload view before each test:

```
beforeEach(async () => {
 await device.reloadReactNative();
});
```

Use different directory to save screenshot to

```
--artifacts-location android/fastlane/metadata/android/fi-FI/images/
```

Within your test case

```
device.takeScreenshot(‘testname);
```

Detox does not seem to work with Android SDK higher than 27, this is set in android/build.gradle -file.

## Sources

[Detox introduction for Android](https://github.com/wix/Detox/blob/master/docs/Introduction.Android.md)

[Detox screenshot (Wix GitHub)](https://github.com/wix/Detox/tree/master/docs)

[v27 vs v28 [DetoxServer.js/CANNOT_FORWARD] role=testee not connected -error](https://github.com/wix/Detox/issues/1716)

[Google Play Store screenshot guide](https://thetool.io/2019/app-store-google-play-screenshot-sizes-guidelines)
