## Semantic Release

Semantic release can be utilized inside a Ci pipeline to create a semver based versioning for your project. Various plugins are available and can be found at: <https://github.com/semantic-release/semantic-release>.

Semantic-release ca be used to create a CHANGELOG.md file, update package.json version and add a tag with release notes to each of our git pushes.

Install semantic-release plugin via npm:
`npm install --save-dev semantic-release`

Also install semantic-release specific plugins to the enviroment of your choice.

```
@semantic-release/changelog
@semantic-release/commit-analyzer
@semantic-release/git
@semantic-release/npm
@semantic-release/github or @semantic-release/gitlab
@semantic-release/release-notes-generator
```

- Install commit-analyzer:
  `npm install --save-dev @semantic-release/commit-analyzer`

- Install release-notes-generator: `npm install --save-dev @semantic-release/release-notes-generator`

- Install changelog: `npm install --save-dev @semantic-release/changelog`

- Install git tools `npm install --save-dev @semantic-release/git`

FOR GITHUB:

- Install Github sepcific tools `npm install --save-dev @semantic-release/github`

FOR GITLAB

- Install GitLab specific tools `npm install --save-dev @semantic-release/gitlab`

- Install shareable configuration for GitLab, this allows the use of commit messages that will not trigger a Ci. eg. chore. `npm install --save-dev @semantic-release/gitlab-config`

Finally, add the code below into your package.json file. Change the branch-name to whatever branch you intend to use the semantic-release on, also don't forget to update the git repository URL.

> You can specify what branch semantic-release should be executed on using a cli option as well: `npx semantic-release --branch BRANCH_NAME`

```
  "release": {
    "branch": "master",
    "repositoryUrl": "git@gitlab.com:niki.baranov/awesomeproject.git",
    "extends": "@semantic-release/gitlab-config",
    "plugins": [
      "@semantic-release/commit-analyzer",
      "@semantic-release/release-notes-generator",
      "@semantic-release/changelog",
      "@semantic-release/npm",
      "@semantic-release/git",
      "@semantic-release/gitlab"
    ]
  }
```

> The configuration above is specifically for GitLab, if you intend to use semantic-release for other Git system, you will need to install that system specific plugin (and change the repo URL...). The plugins should be in this exact order.

> For use with GitHub, you can remove the `extends` part and replace `semantic-release/gitlab` with `semantic-release/github`

To configure semantic-release to work with your Ci, check semantic-release docs: <https://github.com/semantic-release/semantic-release/blob/master/docs/recipes/README.md#git-hosted-services>

## Connect semantic-release with Git

You will also need to setup a **GL_TOKEN** (for GitLab) and **GH_TOKEN** (for GitHub) environment variables with access token information.

For GitLab this is in Settings -> CI / CD -> Variables (Create the token from Profile Settings > Access Tokens, use `api` and `write repository` -rights).

GitHub does not have its own CI, so you will need to setup the GH_TOKEN in whichever CI you intend to use. Setup the GitHub access token from Profile settings -> Developer settings -> Personal Access tokens -> Generate new token.

Once you have the token setup in your CI, you can create the Ci file and initialize semantic-release by using `npx semantic-release`.

> Since semantic-release is a npm package, you will need to run `npm install` before initiating it in CI.

If your configuration was correct, your next commit (breaking or feature or fix) should generate a new TAG for your release, CHANGELOG.md file with the notes as well as update the package.json version.

> Keep in mind that the CHANGELOG.md as well as other version related changes will only happen upon a major/minor/fix type of release, as other commits will not trigger version update.

## Sources

[Semantic Release](https://semantic-release.gitbook.io/semantic-release)

[Semantic Release plugin for npm (semantic-release @GitHub)](https://github.com/semantic-release/semantic-release)

[Semantic Release Docs](https://github.com/semantic-release/semantic-release/blob/master/docs/usage/plugins.md)
