# Commitizen

Commitizen is a useful tool if you want to keep your commit messages in "control" and apply the conventional commit and semver conventions in your projects. Based on commit messages that commitizen will enforce you can create a working Ci pipeline actions.

Install commitizen using:

`npm install --save-dev commitizen`

Initialize commitzen to use conventional-changelog with:

`commitizen init cz-conventional-changelog --save-dev`

Now install the git-cz::

`npm install --save-dev git-cz`

To use commitizen simply use `git cz` and select the type of commit you are making. Commitizen uses BREAKING, FEATURE, FIX for MAJOR, MINOR and FIX (breaking.feature.fix = major.minor.fix).

Commitizen has various plugins (adapters), including ones that allow for custom messages. <https://github.com/commitizen/cz-cli#adapters>

Make sure you have this in the package.json:

```
  "config": {
    "commitizen": {
      "path": "./node_modules/cz-conventional-changelog"
    }
  }
```

## Sources

[Commitizen adapters (commitizen @GitHub)](https://github.com/commitizen/cz-cli#adapters)

[Commitizen plugin for npm (commitizen @GitHub)](https://github.com/commitizen/cz-cli)

[Git-cz plugin for npm (npmjs)](https://www.npmjs.com/package/git-cz)

[Commitlint plugin for npm (conventional-changelog @GitHub)](https://github.com/conventional-changelog/commitlint)

[Conventional changelog plugin for npm (conventional-changelog @GitHub)](https://github.com/conventional-changelog/conventional-changelog)

[Conventional commits detector plugin for npm (conventional-changelog @GitHub)](https://github.com/conventional-changelog/conventional-commits-detector)
