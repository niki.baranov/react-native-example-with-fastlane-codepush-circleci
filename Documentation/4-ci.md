# CI integration for Fastlane and Codepush

CICD pipeline using fastlane and codepush requires unique configuration for each CI system you use, otherwise no other changes are required and any fastlane instructions written for one CI will work for any other.

## CircleCI

Full .yml file for use with CircleCI:

```
# Javascript Node CircleCI 2.0 configuration file
#
# Check https://circleci.com/docs/2.0/language-javascript/ for more details
#
version: 2

references:
  workspace: &workspace ~/CiTestApp

  gradle_key: &gradle_key jars-{{ checksum "android/gradle/wrapper/gradle-wrapper.properties" }}-{{ checksum "android/build.gradle" }}-{{ checksum "android/app/build.gradle" }}

  gem_key: &gems_key v1-gems-deps-{{ checksum "Gemfile.lock" }}

  npm_json: &npm_json v1-npm-deps-{{ checksum "package.json" }}

  restore_gradle_cache: &restore_gradle_cache
    restore_cache:
      name: Restore gradle cache
      key: *gradle_key

  restore_gems_cache: &restore_gems_cache
    restore_cache:
      name: Restore Ruby/gem cache
      key: *gems_key

  restore_npm_cache: &restore_npm_cache
    restore_cache:
      name: Restore NPM cache
      key: *npm_json

  save_gradle_cache: &save_gradle_cache
    save_cache:
      name: Save Gradle cache
      key: *gradle_key
      paths:
        - ~/android/.gradle
        - ~/.m2

  save_gems_cache: &save_gems_cache
    save_cache:
      name: Save Ruby/gems cache
      key: *gems_key
      paths:
        - ~/.bundle

  save_npm_cache: &save_npm_cache
    save_cache:
      name: Save NPM cache
      key: *npm_json
      paths:
        - node_modules

  ruby_dependencies: &ruby_dependencies
    run:
      name: Download Ruby Dependencies
      command: |
        gem install bundler
        bundle check --path ~/.bundle || bundle install --path ~/.bundle && bundle clean

  npm_dependencies: &npm_dependencies
    run:
      name: Download NPM Dependencies
      command: npm install

  decode_android_key: &decode_android_key
    run:
      name: Decode Android key store
      command: echo $KEYSTORE | base64 -d > android/app/my-upload-key.keystore

  decode_app_secret: &decode_app_secret
    run:
      name: Decode App Center App Secret
      command: mkdir android/app/src/main/assets && echo $APPSECRET | base64 -d > android/app/src/main/assets/appcenter-config.json

workflows:
  version: 2
  release_app:
    jobs:
      # builds / updates alpha branch on git push
      - build:
          filters:
            branches:
              only: alpha

      # promotes alpha release to beta
      - promote:
          filters:
            # do this only for master branch (on merge)
            branches:
              only: master

jobs:
  # run promote job to promote google and codepush updates
  promote:
    working_directory: ~/CiTestApp

    docker:
      - image: circleci/android:api-29-node

    steps:
      - checkout
      - *decode_android_key
      - *decode_app_secret
      - run:
          name: Add beta deployment key to strings.xml
          command: sed -i 's/CODEPUSH_DEPLOYMENT_KEY_HERE/'$BETA_KEY'/g' android/app/src/main/res/values/strings.xml
      - *restore_gradle_cache
      - *restore_gems_cache
      - *restore_npm_cache
      - *ruby_dependencies
      - *npm_dependencies
      - *save_gradle_cache
      - *save_gems_cache
      - *save_npm_cache

      - run:
          name: Promote codepush update or create a new build for beta
          command: bundle exec fastlane promote track_to_promote:alpha track_to_promote_to:beta

  # create a build for alpha track in google or create updates for codepush
  build:
    working_directory: ~/CiTestApp

    docker:
      # Specify service dependencies here if necessary
      # CircleCI maintains a library of pre-built images
      # documented at https://circleci.com/docs/2.0/circleci-images/
      - image: circleci/android:api-29-node

    steps:
      - checkout
      - *decode_android_key
      - *decode_app_secret
      - run:
          name: Add alpha deployment key  to strings.xml
          command: sed -i 's/CODEPUSH_DEPLOYMENT_KEY_HERE/'$ALPHA_KEY'/g' android/app/src/main/res/values/strings.xml
      - *restore_gradle_cache
      - *restore_gems_cache
      - *restore_npm_cache
      - *ruby_dependencies
      - *npm_dependencies
      - *save_gradle_cache
      - *save_gems_cache
      - *save_npm_cache

      # Run semantic release to create a CHANGELOG.md file, update package.json version an create release tag
      # Is set to run on alpha branch only
      - run:
          name: Running semantic-release
          command: npx semantic-release --branch $CIRCLE_BRANCH

      # run fastlane to check which update to run
      # release: pro=Release,st=Staging,dev=development
      # track: name of google play track
      - run:
          name: Running Fastlane to create codepush update or new build
          command: bundle exec fastlane update release:pro track:$CIRCLE_BRANCH
```

## GitLabCI

- Current setup of GitLabCI does not utilize cache
- Currently automation of push to master does not work as intended.

```
# setup image to use for all stages

stages:
  - build
  - promote

default:
  before_script:
    - gem install bundler
    - bundle check --path ~/.bundle || bundle install --path ~/.bundle && bundle clean
    - npm install
    - pwd
    #- mkdir -p android/app/src/main/assets && echo base64 -w 0 $APPSECRET | base64 -d > android/app/src/main/assets/appcenter-config.json
    - echo $KEYSTORE | base64 -d > android/app/my-upload-key.keystore

promote:
  only:
    - master
    - merge_requests
  except:
    - alpha
    - pushes
  image: circleci/android:api-29-node
  stage: promote
  script:
    - sed -i 's/CODEPUSH_DEPLOYMENT_KEY_HERE/'$BETA_KEY'/g' android/app/src/main/res/values/strings.xml
    - bundle exec fastlane promote track_to_promote:alpha track_to_promote_to:beta

release_update:
  only:
    - alpha
    - pushes
  except:
    - master
    - merge_requests
  image: circleci/android:api-29-node
  stage: build
  script:
    # semantic release should only be done on non-master track
    - npx semantic-release --branch $CI_COMMIT_REF_NAME
    - sed -i 's/CODEPUSH_DEPLOYMENT_KEY_HERE/'$ALPHA_KEY'/g' android/app/src/main/res/values/strings.xml
    - bundle exec fastlane update release:pro track:$CI_COMMIT_REF_NAME
```

## Pipeline explained

Build pipeline is responsible for creating initial ALPHA build of the app and uploading it to Google Play Store, if needed it will also handle smaller updates releases to codePush.

![Build pipeline](src/citestapp_update_lane.png)

Promote pipeline is responsible for creating a new builds for master branch as well as promoting codePush updates that were previously created by update lane.

![Promote pipeline](src/citestapp_promote_lane.png)

The pipelines are quite similar with the main difference being that semantic-release is not being executed during `promote`, this is because the intent is to avoid doing any direct changes to the master branch and instead doing development on alpha branch and then using `git merge` with `git push` as the trigger for creationg of new master release once all the tests are done.

Having the pipeline setup like this prevents untested and unverified updates from being deployed to production, as promote pipeline will only attempt to promote already existing codePush updates from Alpha deployment and since no new tags are being created during promote lane, the deployment to google will fail because package.json version is the same as the Version name.

## Things to note

- To compile and build iOS builds on GitLab, you will need to use external services / runners.

- CircleCI offers android as well as iOS runners to use, iOS ones are a bit more expensive.

- While you can use CircleCI to build your android app, you will NOT be able to run AVD emulators as it currently does not support x86 architecture: [Circle CI support Jan 31. 2019](https://support.circleci.com/hc/en-us/articles/360000028928-Testing-with-Android-emulator-on-CircleCI-2-0).

## Sources

[CircleCI Language-ruby (CircleCi)](https://circleci.com/docs/2.0/language-ruby/)

[Caching with CircleCI (CircleCI)](https://circleci.com/docs/2.0/caching/)

[Cache ruby gems (CircleCI)](https://discuss.circleci.com/t/caching-ruby-gems/21868/11)

[Gradle cache (Gradle)](https://docs.gradle.org/current/userguide/build_cache.html)

[Fastlane + CircleCI for Android — Part 1 (Medium)](https://medium.com/open-knowledge/fastlane-circleci-for-android-part-1-471e29b968b2)

[Fastlane + CircleCI for Android — Part 2 (Medium)](https://medium.com/open-knowledge/fastlane-circleci-for-android-part-2-3dedadac89c2)

[Fastlane + CircleCI for Android — Part 3 (Medium)](https://medium.com/open-knowledge/fastlane-circleci-for-android-part-3-f2282bc30571)

[CircleCi nodejs images (CircleCi)](https://circleci.com/docs/2.0/circleci-images/#nodejs)

[Docker images tags (CircleCi)](https://circleci.com/docs/2.0/docker-image-tags.json)

[Language Guide: Android (CircleCi)](https://circleci.com/docs/2.0/language-android/)

[Testing with Android emulator on CircleCI 2.0 (CircleCi)](https://support.circleci.com/hc/en-us/articles/360000028928-Testing-with-Android-emulator-on-CircleCI-2-0)

[Emulator not launching… forever (CircleCi)](https://discuss.circleci.com/t/emulator-not-launching-forever/15892/2)

[facebookincubator (CircleCi)](https://circleci.com/gh/facebookincubator/spectrum/294)

[Inventory Agent for Android (flyve-mdm @GitHub)](https://github.com/flyve-mdm/android-inventory-agent)

[How to run Android UI tests properly on CircleCI 2.0? (StackOverflow)](https://stackoverflow.com/questions/45424110/how-to-run-android-ui-tests-properly-on-circleci-2-0)

[Run Android emulator on CircleCI 2.0 macOS (DoguD @Github)](https://gist.github.com/DoguD/58b4b86a5d892130af84074078581b87)

[CircleCI with Android (proandroiddev)](https://proandroiddev.com/circleci-with-android-continuous-integration-3ecd98f92bd4)

[Android Continuous Integration using Fastlane and CircleCI 2.0 — Part I (Medium)](https://medium.com/pink-room-club/android-continuous-integration-using-fastlane-and-circleci-2-0-part-i-7204e2e7b8b)

[Android Continuous Integration using Fastlane and CircleCI 2.0 — Part 2 (Medium)](https://medium.com/pink-room-club/android-continuous-integration-using-fastlane-and-circleci-2-0-part-ii-7f8dd7265659)
