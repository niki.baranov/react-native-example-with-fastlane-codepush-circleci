## [8.0.4](https://gitlab.com/shareway/helloworldapp/compare/v8.0.3...v8.0.4) (2019-12-10)


### Bug Fixes

* 🐛 Yet another CI test ([849a133](https://gitlab.com/shareway/helloworldapp/commit/849a1339a37d8d85c117579c17abd12d3bb4a66c))

## [8.0.3](https://gitlab.com/shareway/helloworldapp/compare/v8.0.2...v8.0.3) (2019-12-03)


### Bug Fixes

* 🐛 One more test, removing duplicate current version in app ([b82bdf6](https://gitlab.com/shareway/helloworldapp/commit/b82bdf6331d09c55f519b4695971e6def07c5209))

## [8.0.2](https://gitlab.com/shareway/helloworldapp/compare/v8.0.1...v8.0.2) (2019-12-03)


### Bug Fixes

* 🐛 Testing another alpha update ([7a9f41b](https://gitlab.com/shareway/helloworldapp/commit/7a9f41b967a7f20415b573f765dfba263225c8b7))

## [8.0.1](https://gitlab.com/shareway/helloworldapp/compare/v8.0.0...v8.0.1) (2019-12-03)


### Bug Fixes

* 🐛 Testing CI from gitlab ([02ec4bf](https://gitlab.com/shareway/helloworldapp/commit/02ec4bf3602000260dc25b2ea37d4774f84d9a60))

# [8.0.0](https://github.com/rennehir/CiTestApp/compare/v7.0.3...v8.0.0) (2019-12-02)


### Features

* 🎸 Adding current version app check via codepush ([ba8707d](https://github.com/rennehir/CiTestApp/commit/ba8707d27271bf893424ec9e6494f21a555b1375))


### BREAKING CHANGES

* Adding current app version check

## [7.0.3](https://github.com/rennehir/CiTestApp/compare/v7.0.2...v7.0.3) (2019-12-02)


### Bug Fixes

* 🐛 Testing ci for fix update ([6ef0c3b](https://github.com/rennehir/CiTestApp/commit/6ef0c3b826c22f00dbb64543ec0c0cd4e383e777))

## [7.0.2](https://github.com/rennehir/CiTestApp/compare/v7.0.1...v7.0.2) (2019-12-01)


### Bug Fixes

* 🐛 Figuring out why master branch update fails ([fc2f496](https://github.com/rennehir/CiTestApp/commit/fc2f49639f1e9802f8be19b62e414b253dae6d8d))

## [7.0.1](https://github.com/rennehir/CiTestApp/compare/v7.0.0...v7.0.1) (2019-12-01)


### Bug Fixes

* 🐛 Mergin master to alpha, small update to test codepush ([df63d43](https://github.com/rennehir/CiTestApp/commit/df63d43142f4f4d5ff79dba0bd6410b11073a90f))

# [7.0.0](https://github.com/rennehir/CiTestApp/compare/v6.0.0...v7.0.0) (2019-12-01)


### Code Refactoring

* 💡 Fixing the build ([c97aece](https://github.com/rennehir/CiTestApp/commit/c97aece2f44e2187bc4dedccaad6c93fa19979d8))


### BREAKING CHANGES

* Checking full ci process

# [6.0.0](https://github.com/rennehir/CiTestApp/compare/v5.0.0...v6.0.0) (2019-11-30)


### Bug Fixes

* 🐛 Changing gradle clean config to be done for all builds ([76d0173](https://github.com/rennehir/CiTestApp/commit/76d0173508339c6c78a2423859e6c667be39b6a4))


### BREAKING CHANGES

* New build to correct errors

# [5.0.0](https://github.com/rennehir/CiTestApp/compare/v4.2.1...v5.0.0) (2019-11-30)


### Code Refactoring

* 💡 New build because previous did not work ([4638c39](https://github.com/rennehir/CiTestApp/commit/4638c39730e01b49d09d80ffe803f1e24efddd67))


### BREAKING CHANGES

* Correcting errors in old build

## [4.2.1](https://github.com/rennehir/CiTestApp/compare/v4.2.0...v4.2.1) (2019-11-30)


### Bug Fixes

* 🐛 Fixing codepush version checkup logic in app.js ([ac5eda1](https://github.com/rennehir/CiTestApp/commit/ac5eda19495caecb7fb83a23bcb3ad672f7435ba))

# [4.2.0](https://github.com/rennehir/CiTestApp/compare/v4.1.1...v4.2.0) (2019-11-29)


### Features

* 🎸 Testing ci ([16898a1](https://github.com/rennehir/CiTestApp/commit/16898a142603f106ad653392f677865be4c5f288))

## [4.1.1](https://github.com/rennehir/CiTestApp/compare/v4.1.0...v4.1.1) (2019-11-29)


### Bug Fixes

* 🐛 Testing ci some more ([b479d15](https://github.com/rennehir/CiTestApp/commit/b479d1526fa5b17d13b7a2912ee8e37942a79a42))

# [4.1.0](https://github.com/rennehir/CiTestApp/compare/v4.0.0...v4.1.0) (2019-11-29)


### Features

* 🎸 Testing codepush sync ([abfaa91](https://github.com/rennehir/CiTestApp/commit/abfaa9178ceadad9425e8b62171ae9ace74e0d19))

# [4.0.0](https://github.com/rennehir/CiTestApp/compare/v3.3.4...v4.0.0) (2019-11-29)


### Code Refactoring

* 💡 Changing deployment key setting in ci ([1912606](https://github.com/rennehir/CiTestApp/commit/1912606a9db658677c23c93f7086522198d4b1d0))


### BREAKING CHANGES

* CI logic

## [3.3.4](https://github.com/rennehir/CiTestApp/compare/v3.3.3...v3.3.4) (2019-11-29)


### Bug Fixes

* 🐛 Testing cpdkey content in ci ([c06e213](https://github.com/rennehir/CiTestApp/commit/c06e213da40626c05b2ab8b29d246b36576314e0))

## [3.3.3](https://github.com/rennehir/CiTestApp/compare/v3.3.2...v3.3.3) (2019-11-29)


### Bug Fixes

* 🐛 Testing ci env var ([79c52a2](https://github.com/rennehir/CiTestApp/commit/79c52a210639fdbb738ef680b4a48dd8e95f3ab7))

## [3.3.2](https://github.com/rennehir/CiTestApp/compare/v3.3.1...v3.3.2) (2019-11-29)


### Bug Fixes

* 🐛 Testing fix ci ([0ac0288](https://github.com/rennehir/CiTestApp/commit/0ac0288dc89b39ce6cc5cd82437599f4e73fcd43))

## [3.3.1](https://github.com/rennehir/CiTestApp/compare/v3.3.0...v3.3.1) (2019-11-28)


### Bug Fixes

* 🐛 Fix message test in ci ([3e9107d](https://github.com/rennehir/CiTestApp/commit/3e9107df445d1d5f365eb40e867348a422e0beb3))

# [3.3.0](https://github.com/rennehir/CiTestApp/compare/v3.2.0...v3.3.0) (2019-11-28)


### Features

* 🎸 Fixing description value for codepush ([e604cac](https://github.com/rennehir/CiTestApp/commit/e604cacb679955e688444cf86551849f79266824))

# [3.2.0](https://github.com/rennehir/CiTestApp/compare/v3.1.4...v3.2.0) (2019-11-28)


### Features

* 🎸 testing feat update to see that the description is ok ([22d9975](https://github.com/rennehir/CiTestApp/commit/22d997576c8951a5cd1a4ee6ed3d2aa0d4fc43a3))

## [3.1.4](https://github.com/rennehir/CiTestApp/compare/v3.1.3...v3.1.4) (2019-11-28)


### Bug Fixes

* 🐛 Small fix to init codepush with a description ([31c202b](https://github.com/rennehir/CiTestApp/commit/31c202b67ed006051fddb81e850fbdc8a067984d))

## [3.1.3](https://github.com/rennehir/CiTestApp/compare/v3.1.2...v3.1.3) (2019-11-28)


### Bug Fixes

* 🐛 Fixing description tag for codepush update ([a3f8e29](https://github.com/rennehir/CiTestApp/commit/a3f8e29a2a04e83649591c7863456cf5937a1fd6))

## [3.1.2](https://github.com/rennehir/CiTestApp/compare/v3.1.1...v3.1.2) (2019-11-28)


### Bug Fixes

* 🐛 Fixed typo in fastfile for codepush update message ([3ff6913](https://github.com/rennehir/CiTestApp/commit/3ff69131c55638cabde243b7c067ca4ff022ad4b))

## [3.1.1](https://github.com/rennehir/CiTestApp/compare/v3.1.0...v3.1.1) (2019-11-28)


### Bug Fixes

* 🐛 Adding some descriptions to codepush update ([6746cd0](https://github.com/rennehir/CiTestApp/commit/6746cd09662c5251d9f0a22ff8d088d4508b1170))

# [3.1.0](https://github.com/rennehir/CiTestApp/compare/v3.0.0...v3.1.0) (2019-11-28)


### Features

* 🎸 Testing mandatory codepush update in ci ([1cb5ed0](https://github.com/rennehir/CiTestApp/commit/1cb5ed0833d420ff6a3d78a3062c72fe18a5c174))

# [3.0.0](https://github.com/rennehir/CiTestApp/compare/v2.2.5...v3.0.0) (2019-11-28)


### Features

* 🎸 Total change of epic proportions! ([2f5335d](https://github.com/rennehir/CiTestApp/commit/2f5335d751886697f77cad09da114fde10d2a112))


### BREAKING CHANGES

* EVERYTHING

## [2.2.5](https://github.com/rennehir/CiTestApp/compare/v2.2.4...v2.2.5) (2019-11-28)


### Bug Fixes

* 🐛 Testing using circleci CIRCLE_BRANCH variable in ci ([5a33460](https://github.com/rennehir/CiTestApp/commit/5a33460d96089d4f8a600ca374f4d173db784b44))

## [2.2.4](https://github.com/rennehir/CiTestApp/compare/v2.2.3...v2.2.4) (2019-11-28)


### Bug Fixes

* 🐛 Setting branch via cli argument in ci config.yml ([f1efb37](https://github.com/rennehir/CiTestApp/commit/f1efb370f126a3608f4e3f2d99aa4025efc1892e))

## [2.2.3](https://github.com/rennehir/CiTestApp/compare/v2.2.2...v2.2.3) (2019-11-28)


### Bug Fixes

* 🐛 Setting semantic release to use multiple branches ([c0ab829](https://github.com/rennehir/CiTestApp/commit/c0ab829ee8b30ce09efa7cab6bd41e56cb84a22f))
* 🐛 Setting up ci based on branch ([22f8ceb](https://github.com/rennehir/CiTestApp/commit/22f8cebd38a845dcbfdf5f2a467145b324f44cd4))
* 🐛 Testing ci by branch ([79de346](https://github.com/rennehir/CiTestApp/commit/79de346308bcb3231dcabeeb51bf4a0e09bce6a7))

## [2.2.2](https://github.com/rennehir/CiTestApp/compare/v2.2.1...v2.2.2) (2019-11-27)


### Bug Fixes

* 🐛 Testing rollback by breaking button title ([1505643](https://github.com/rennehir/CiTestApp/commit/150564363014b0582b7c3cd718120037402c0396))

## [2.2.1](https://github.com/rennehir/CiTestApp/compare/v2.2.0...v2.2.1) (2019-11-27)


### Bug Fixes

* 🐛 Testing rollback for a fix/patch update ([c9ea639](https://github.com/rennehir/CiTestApp/commit/c9ea639e280843f3fb890e6f2d4351bc630788ad))

# [2.2.0](https://github.com/rennehir/CiTestApp/compare/v2.1.0...v2.2.0) (2019-11-27)


### Features

* 🎸 Testing rollback by uploading a broken minor update ([03dc685](https://github.com/rennehir/CiTestApp/commit/03dc685ed1ab61ddcef7190d52b08c6b5f28454e))

# [2.1.0](https://github.com/rennehir/CiTestApp/compare/v2.0.2...v2.1.0) (2019-11-27)


### Features

* 🎸 Testing minor/feat update via ci ([ad625fa](https://github.com/rennehir/CiTestApp/commit/ad625fae4f7d76e4d562c92ca97d7cfb6f15da84))

## [2.0.2](https://github.com/rennehir/CiTestApp/compare/v2.0.1...v2.0.2) (2019-11-27)


### Bug Fixes

* 🐛 Testing fix/patch ci pipe ([516bfd4](https://github.com/rennehir/CiTestApp/commit/516bfd47a8fed599ce87bc66077e1423e83c226c))

## [2.0.1](https://github.com/rennehir/CiTestApp/compare/v2.0.0...v2.0.1) (2019-11-26)


### Bug Fixes

* 🐛 Checking fix detection in ci ([90a811b](https://github.com/rennehir/CiTestApp/commit/90a811b06c3cc67dee28c749bf2d06bf84d2df27))

# [2.0.0](https://github.com/rennehir/CiTestApp/compare/v1.4.0...v2.0.0) (2019-11-26)


### Features

* 🎸 Adding codepush to ci via fastlane checks ([9a375c4](https://github.com/rennehir/CiTestApp/commit/9a375c42e0fca3d7f41de31965f783a63568717f))


### BREAKING CHANGES

* Ci and codepush integration

# [1.4.0](https://github.com/rennehir/CiTestApp/compare/v1.3.28...v1.4.0) (2019-11-20)


### Features

* 🎸 Adding Codepush, updating version ([c1fcf66](https://github.com/rennehir/CiTestApp/commit/c1fcf668f6bcc27bf574057e895345da552c43b7))

## [1.3.28](https://github.com/rennehir/CiTestApp/compare/v1.3.27...v1.3.28) (2019-11-14)


### Bug Fixes

* 🐛 testing gradle cache ([aa268c2](https://github.com/rennehir/CiTestApp/commit/aa268c2f028ee638d1ed258bc83d07c0afc70b96))

## [1.3.27](https://github.com/rennehir/CiTestApp/compare/v1.3.26...v1.3.27) (2019-11-14)


### Bug Fixes

* 🐛 removing run: echo "All done" from ci ([022d261](https://github.com/rennehir/CiTestApp/commit/022d26117af6e28681c0af375b74bd1b1cda124f))

## [1.3.26](https://github.com/rennehir/CiTestApp/compare/v1.3.25...v1.3.26) (2019-11-14)


### Bug Fixes

* 🐛 building caches  for gradle,ruby,npm ([04cb6a9](https://github.com/rennehir/CiTestApp/commit/04cb6a918d13af04ca9ee8737e120fab8361338b))

## [1.3.25](https://github.com/rennehir/CiTestApp/compare/v1.3.24...v1.3.25) (2019-11-14)


### Bug Fixes

* 🐛 Trying to get automated json test ([7730189](https://github.com/rennehir/CiTestApp/commit/7730189410d2273288dad373821a5eac97b8d735))

## [1.3.24](https://github.com/rennehir/CiTestApp/compare/v1.3.23...v1.3.24) (2019-11-14)


### Bug Fixes

* 🐛 Removing automated json key test ([451e9fa](https://github.com/rennehir/CiTestApp/commit/451e9faae925a2030bb368a55a2282a2a3f60883))

## [1.3.23](https://github.com/rennehir/CiTestApp/compare/v1.3.22...v1.3.23) (2019-11-14)


### Bug Fixes

* 🐛 fixing automating json key checking ([cc8756e](https://github.com/rennehir/CiTestApp/commit/cc8756e3abc119de807bd7edbc5ed202304707ee))

## [1.3.22](https://github.com/rennehir/CiTestApp/compare/v1.3.21...v1.3.22) (2019-11-14)


### Bug Fixes

* 🐛 Automate json key check for local or set env ([cb26421](https://github.com/rennehir/CiTestApp/commit/cb26421ff69fd4b2320afd6b762d93668dffab9e))

## [1.3.21](https://github.com/rennehir/CiTestApp/compare/v1.3.20...v1.3.21) (2019-11-14)


### Bug Fixes

* 🐛 Testing running ci with fastlane in root ([7504b24](https://github.com/rennehir/CiTestApp/commit/7504b240564c95fbf818ba8dd89b5992ceb12877))

## [1.3.20](https://github.com/rennehir/CiTestApp/compare/v1.3.19...v1.3.20) (2019-11-14)


### Bug Fixes

* 🐛 Moving fastlane to project root ([5b033d0](https://github.com/rennehir/CiTestApp/commit/5b033d0d63cfd4906dd7177d519b3a0dc245f76f))

## [1.3.19](https://github.com/rennehir/CiTestApp/compare/v1.3.18...v1.3.19) (2019-11-13)


### Bug Fixes

* 🐛 setting default abi for avd ([7913633](https://github.com/rennehir/CiTestApp/commit/79136337cebefae2b8dc1869b68954fe0dc16a8a))

## [1.3.18](https://github.com/rennehir/CiTestApp/compare/v1.3.17...v1.3.18) (2019-11-13)


### Bug Fixes

* 🐛 verifying android sdk and amulator creation in ci ([92c9660](https://github.com/rennehir/CiTestApp/commit/92c9660d8d97aeb473af70a8bb42409dd77fee6f))

## [1.3.17](https://github.com/rennehir/CiTestApp/compare/v1.3.16...v1.3.17) (2019-11-13)


### Bug Fixes

* 🐛 accepting skd licence, adding sdk image for emulator ([9caf131](https://github.com/rennehir/CiTestApp/commit/9caf1311b20ccabdce396a6cd14a1234e9354724))

## [1.3.16](https://github.com/rennehir/CiTestApp/compare/v1.3.15...v1.3.16) (2019-11-13)


### Bug Fixes

* 🐛 Reconfiguring avd creation command in ci ([acaf0fa](https://github.com/rennehir/CiTestApp/commit/acaf0faa8228a621d1b0d9475a755cc2d1bb2895))

## [1.3.15](https://github.com/rennehir/CiTestApp/compare/v1.3.14...v1.3.15) (2019-11-13)


### Bug Fixes

* 🐛 Adding AVD creation to ci Nexus 5x test ([166ebf5](https://github.com/rennehir/CiTestApp/commit/166ebf5a5fe969b203993aa29186efd3ae5e1a98))
* 🐛 Adding AVD creation to CI, creating nexus 5x test ([52d89ff](https://github.com/rennehir/CiTestApp/commit/52d89ff68bdd5056c5989f3937c9010408a6350b))

## [1.3.14](https://github.com/rennehir/CiTestApp/compare/v1.3.13...v1.3.14) (2019-11-12)


### Bug Fixes

* 🐛 Fixing npm script command in config.yml ci ([d347c9b](https://github.com/rennehir/CiTestApp/commit/d347c9b67c9f8ff8fd39e8d5b0c1d97390fd8ac7))

## [1.3.13](https://github.com/rennehir/CiTestApp/compare/v1.3.12...v1.3.13) (2019-11-12)


### Bug Fixes

* 🐛 Adding npm scripts to package json ([5ac65dd](https://github.com/rennehir/CiTestApp/commit/5ac65ddd96e68581ba95c5ea2864023cf2165aea))

## [1.3.12](https://github.com/rennehir/CiTestApp/compare/v1.3.11...v1.3.12) (2019-11-12)


### Bug Fixes

* 🐛 it works again ([9bbba0e](https://github.com/rennehir/CiTestApp/commit/9bbba0eeb219f15100191bf82ff520e5795e186c))
* 🐛 re uploading after removinng node_modules ([38218c2](https://github.com/rennehir/CiTestApp/commit/38218c2ef30627862d5d75c0b4e9fdf8ba1cb697))

## [1.3.11](https://github.com/rennehir/CiTestApp/compare/v1.3.10...v1.3.11) (2019-11-12)


### Bug Fixes

* 🐛 re-enabling full ci without fastline ([43c6e12](https://github.com/rennehir/CiTestApp/commit/43c6e12d6ba37d7f23dbc097e0c1af0329a96a81))

## [1.3.10](https://github.com/rennehir/CiTestApp/compare/v1.3.9...v1.3.10) (2019-11-12)


### Bug Fixes

* 🐛 removing detox init mocha. running detox without npx ([dbcb85d](https://github.com/rennehir/CiTestApp/commit/dbcb85d6f46268b4579c4a5406dc9787d3649574))

## [1.3.9](https://github.com/rennehir/CiTestApp/compare/v1.3.8...v1.3.9) (2019-11-12)


### Bug Fixes

* 🐛 run detox with npx ([701097d](https://github.com/rennehir/CiTestApp/commit/701097dc66b415210b48d377c92fc0c43696dc1a))

## [1.3.8](https://github.com/rennehir/CiTestApp/compare/v1.3.7...v1.3.8) (2019-11-12)


### Bug Fixes

* 🐛 fixing dependencies ([52a7c7e](https://github.com/rennehir/CiTestApp/commit/52a7c7e8091d77b1108cb83ec53c3e1789e2f42d))

## [1.3.7](https://github.com/rennehir/CiTestApp/compare/v1.3.6...v1.3.7) (2019-11-12)


### Bug Fixes

* 🐛 added detox cli as dependency with yarn ([cf4be04](https://github.com/rennehir/CiTestApp/commit/cf4be04de6608abd9cbe1ead70ee0493ab4257fe))

## [1.3.6](https://github.com/rennehir/CiTestApp/compare/v1.3.5...v1.3.6) (2019-11-12)


### Bug Fixes

* 🐛 install detox-cli without -g ([cc4cde7](https://github.com/rennehir/CiTestApp/commit/cc4cde76109b37ff172ae01ff7eea40ac1879eb4))
* 🐛 installing detox-cli as a separate step with -g ([8cd3250](https://github.com/rennehir/CiTestApp/commit/8cd325049928840bc5e41d0f39848e384832f1a7))

## [1.3.5](https://github.com/rennehir/CiTestApp/compare/v1.3.4...v1.3.5) (2019-11-12)


### Bug Fixes

* 🐛 adding detox cli to package.json ([93da6d7](https://github.com/rennehir/CiTestApp/commit/93da6d72f3b9ca8bd826bffedc536ab4c704a31f))
* 🐛 adding detox-cli to package.json ([2b970ed](https://github.com/rennehir/CiTestApp/commit/2b970ed24625eb7d9e328345e8a21bcea5b88a5a))

## [1.3.4](https://github.com/rennehir/CiTestApp/compare/v1.3.3...v1.3.4) (2019-11-12)


### Bug Fixes

* 🐛 running npm install for dependencies as a step ([85c09d3](https://github.com/rennehir/CiTestApp/commit/85c09d38cb03bf05aeb66bcc7b30722a3aebd513))

## [1.3.3](https://github.com/rennehir/CiTestApp/compare/v1.3.2...v1.3.3) (2019-11-12)


### Bug Fixes

* 🐛 Remove npm caching from config.yml ([f090e2b](https://github.com/rennehir/CiTestApp/commit/f090e2b5ce60ab4fb742a6144b69751296673cfe))

## [1.3.2](https://github.com/rennehir/CiTestApp/compare/v1.3.1...v1.3.2) (2019-11-12)


### Bug Fixes

* 🐛 changing package.json dependency for mocha and detox ([1854be8](https://github.com/rennehir/CiTestApp/commit/1854be8e7375435934825970d23ded05b6756b85))

## [1.3.1](https://github.com/rennehir/CiTestApp/compare/v1.3.0...v1.3.1) (2019-11-12)


### Bug Fixes

* 🐛 changing fastlane appfile json keydata path ([b59a037](https://github.com/rennehir/CiTestApp/commit/b59a03754699a2126e4a8e4ab4ec68c7570935dc))
* 🐛 Detox screenshot ci integration, apk 28 > 29 change ([04cfc43](https://github.com/rennehir/CiTestApp/commit/04cfc43fd14d1c0039b8e887ef2685e2b5d2a910))

# [1.3.0](https://github.com/rennehir/CiTestApp/compare/v1.2.3...v1.3.0) (2019-11-11)


### Features

* 🎸 Adding screenshot generation to CI ([7215af0](https://github.com/rennehir/CiTestApp/commit/7215af04442198eab955b919939fd4f48ec7226b))

## [1.2.3](https://github.com/rennehir/CiTestApp/compare/v1.2.2...v1.2.3) (2019-11-07)


### Bug Fixes

* 🐛 Reuploading new circleci config ([cd74dc8](https://github.com/rennehir/CiTestApp/commit/cd74dc8541550ad8266d1a8287a5c76480489ee9))

## [1.2.2](https://github.com/rennehir/CiTestApp/compare/v1.2.1...v1.2.2) (2019-11-07)


### Bug Fixes

* 🐛 Setting Google store vars ([ce85db9](https://github.com/rennehir/CiTestApp/commit/ce85db9f7bdcf160f4477769cb49f401617987ff))

## [1.2.1](https://github.com/rennehir/CiTestApp/compare/v1.2.0...v1.2.1) (2019-11-07)


### Bug Fixes

* 🐛 Testing full cicd chain ([173bc3f](https://github.com/rennehir/CiTestApp/commit/173bc3fd9c38133c2192c5b9f13f6721ba18293c))

# [1.2.0](https://github.com/rennehir/CiTestApp/compare/v1.1.0...v1.2.0) (2019-11-07)


### Bug Fixes

* 🐛 Creating if loop for updatinng build ([64c4135](https://github.com/rennehir/CiTestApp/commit/64c4135a8059be4aec2b95c4937142f543318037))
* 🐛 dont try to use array as integer ([d2be1c0](https://github.com/rennehir/CiTestApp/commit/d2be1c08d631176c9f191e94f57f7843ab8b52a1))
* 🐛 package.json name to all small letters ([d6465d8](https://github.com/rennehir/CiTestApp/commit/d6465d866444cb070b662ddbbe4418ee94b9cfa6))


### Features

* 🎸 Semantic Release to github ([44a432e](https://github.com/rennehir/CiTestApp/commit/44a432e4063a68c4c86d22f24fc8adac5bbcd015))

## [1.0.2](https://github.com/rennehir/CiTestApp/compare/v1.0.1...v1.0.2) (2019-11-07)


### Bug Fixes

* 🐛 Creating if loop for updatinng build ([64c4135](https://github.com/rennehir/CiTestApp/commit/64c4135a8059be4aec2b95c4937142f543318037))

## [1.0.1](https://github.com/rennehir/CiTestApp/compare/v1.0.0...v1.0.1) (2019-11-07)


### Bug Fixes

* 🐛 package.json name to all small letters ([d6465d8](https://github.com/rennehir/CiTestApp/commit/d6465d866444cb070b662ddbbe4418ee94b9cfa6))

# 1.0.0 (2019-11-07)


### Bug Fixes

* 🐛 adding analyze test ([a0b499a](https://github.com/rennehir/CiTestApp/commit/a0b499a9a6b5e77204872223b9dae90fad19e439))
* 🐛 Adding gitlab-config plugin to semantic-release ([f74ea35](https://github.com/rennehir/CiTestApp/commit/f74ea35221b5323608518d0bd2c4ab57a8a20d88))
* 🐛 Adding semantic-release packages to npm ([f41974a](https://github.com/rennehir/CiTestApp/commit/f41974a0bd8a869b305e4cf7450fb594b69f752b))
* 🐛 Adding semantic-release/git plugin ([90a2658](https://github.com/rennehir/CiTestApp/commit/90a2658086c05f40b7eaaedbe7448fb9fc7b5252))
* 🐛 Fixing branch=next to branch=master ([d65ba67](https://github.com/rennehir/CiTestApp/commit/d65ba67ff01daea963d5a3efcbd5e6cf5b634b5d))
* 🐛 forgot to reinstall @semantic-release/git ([e6a75f4](https://github.com/rennehir/CiTestApp/commit/e6a75f47a2618290d8482220db0279720494b791))
* 🐛 re-adding @semantic-release/git ([3518527](https://github.com/rennehir/CiTestApp/commit/35185271189a2617b69b4b99ed7dc97d4ca8ce66))
* 🐛 remove @semantic-release/git plugin ([09d9232](https://github.com/rennehir/CiTestApp/commit/09d92320688e89370397521fbae86479e3c635a3))
* 🐛 testi taas ([093b60c](https://github.com/rennehir/CiTestApp/commit/093b60c86db1d69958b1cc690428fc4b2c891efd))


### Features

* 🎸 Added fastlane version and build number checks ([f2b24bb](https://github.com/rennehir/CiTestApp/commit/f2b24bbc2d16731e40411e6ae35a9283321b81e3))
* 🎸 Adding commitizen ([083e157](https://github.com/rennehir/CiTestApp/commit/083e1576fd57ae2174255b407f242c8bbf826b51))
* 🎸 Semantic Release to github ([44a432e](https://github.com/rennehir/CiTestApp/commit/44a432e4063a68c4c86d22f24fc8adac5bbcd015))
* 🎸 Yhteys googleen ok ([96ba3b6](https://github.com/rennehir/CiTestApp/commit/96ba3b619eccb9451c230699af97fc252e47db97))

# [1.1.0](https://gitlab.com/shareway/helloworldapp/compare/v1.0.4...v1.1.0) (2019-11-04)


### Features

* 🎸 Added fastlane version and build number checks ([f2b24bb](https://gitlab.com/shareway/helloworldapp/commit/f2b24bbc2d16731e40411e6ae35a9283321b81e3))

## [1.0.4](https://gitlab.com/shareway/helloworldapp/compare/v1.0.3...v1.0.4) (2019-11-01)


### Bug Fixes

* 🐛 testi taas ([093b60c](https://gitlab.com/shareway/helloworldapp/commit/093b60c86db1d69958b1cc690428fc4b2c891efd))

## [1.0.3](https://gitlab.com/shareway/helloworldapp/compare/v1.0.2...v1.0.3) (2019-11-01)


### Bug Fixes

* 🐛 adding analyze test ([a0b499a](https://gitlab.com/shareway/helloworldapp/commit/a0b499a9a6b5e77204872223b9dae90fad19e439))

## [1.0.2](https://gitlab.com/shareway/helloworldapp/compare/v1.0.1...v1.0.2) (2019-10-31)


### Bug Fixes

* 🐛 forgot to reinstall @semantic-release/git ([e6a75f4](https://gitlab.com/shareway/helloworldapp/commit/e6a75f47a2618290d8482220db0279720494b791))
* 🐛 re-adding @semantic-release/git ([3518527](https://gitlab.com/shareway/helloworldapp/commit/35185271189a2617b69b4b99ed7dc97d4ca8ce66))

# 1.0.0 (2019-10-31)


### Bug Fixes

* 🐛 Adding gitlab-config plugin to semantic-release ([f74ea35](https://gitlab.com/shareway/helloworldapp/commit/f74ea35221b5323608518d0bd2c4ab57a8a20d88))
* 🐛 Adding semantic-release packages to npm ([f41974a](https://gitlab.com/shareway/helloworldapp/commit/f41974a0bd8a869b305e4cf7450fb594b69f752b))
* 🐛 Adding semantic-release/git plugin ([90a2658](https://gitlab.com/shareway/helloworldapp/commit/90a2658086c05f40b7eaaedbe7448fb9fc7b5252))
* 🐛 Fixing branch=next to branch=master ([d65ba67](https://gitlab.com/shareway/helloworldapp/commit/d65ba67ff01daea963d5a3efcbd5e6cf5b634b5d))


### Features

* 🎸 Adding commitizen ([083e157](https://gitlab.com/shareway/helloworldapp/commit/083e1576fd57ae2174255b407f242c8bbf826b51))
* 🎸 Yhteys googleen ok ([96ba3b6](https://gitlab.com/shareway/helloworldapp/commit/96ba3b619eccb9451c230699af97fc252e47db97))
