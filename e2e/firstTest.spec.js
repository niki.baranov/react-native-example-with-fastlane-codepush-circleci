describe('CiTestApp ever', () => {
  //name = '0';
  beforeEach(async () => {
    await device.reloadReactNative();
    //  await device.takeScreenshot('before' + name);
  });

  //afterEach(async () => {
  //  await device.takeScreenshot('after' + name);
  //});

  it('phoneScreenshots', async () => {
    //name = '1';
    await expect(element(by.id('welcome'))).toBeVisible();
    await device.takeScreenshot('welcome_screen');

    await element(by.id('hello_button')).tap();
    await expect(element(by.id('shareway-kuva'))).toBeVisible();
    await device.takeScreenshot('button_is_tapped_image_is_visible');
  });

  /*
  it('should have welcome screen', async () => {
    name = '1';
    await expect(element(by.id('welcome'))).toBeVisible();
    await device.takeScreenshot('welcome screen');
  });

  it('should show shareway image after tap', async () => {
    name = '2';
    await device.takeScreenshot('image_is_not_visible');
    await element(by.id('hello_button')).tap();
    await expect(element(by.id('shareway-kuva'))).toBeVisible();
    await device.takeScreenshot('button_is_tapped_image_is_visible');
  });

  it('should not show shareway image after second tap', async () => {
    name = '3';
    await device.takeScreenshot('image_is_not_visible_before_tapping_button');
    await element(by.id('hello_button')).tap();
    await expect(element(by.id('shareway-kuva'))).toBeVisible();
    await device.takeScreenshot('button_is_tapped_image_is_visible');
    await element(by.id('hello_button')).tap();
    await expect(element(by.id('shareway-kuva'))).toBeNotVisible();
    await device.takeScreenshot(
      'image_is_not_visible_after_tapping_button_again',
    );
  });
  */
});
