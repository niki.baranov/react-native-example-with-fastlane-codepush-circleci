/**
 * @format
 */
// codepush
import codePush from 'react-native-code-push';

import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';

import App from './App';

//AppRegistry.registerComponent(appName, () => App);
/*const codePushOptions = {
  checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
  installMode: codePush.InstallMode.ON_NEXT_SUSPEND,
  minimumBackgroundDuration: 0,
};

//const CodePushedApp = codePush(codePushOptions)(App);
*/

codePush.sync(
  {
    updateDialog: false,
    checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
    installMode: codePush.InstallMode.ON_NEXT_RESUME,
    minimumBackgroundDuration: 60 * 2,
  },
  this.onSyncStatusChange,
);

// run react native app through CodePush
const CodePushedApp = codePush(App);
AppRegistry.registerComponent(appName, () => CodePushedApp);
/*
// get info about current device config
codePush.getConfiguration().then(metadata => {
  console.log(metadata.label);
  console.log('appversion: ' + metadata.appVersion);
  console.log(metadata.description);
});

// Get latest update info
codePush.getUpdateMetadata().then(metadata => {
  console.log('appcenter ver: ' + metadata.label);
  console.log('appversion: ' + metadata.appVersion);
  console.log(metadata.description);
});

console.log(codePush.getConfiguration());
console.log(codePush.getUpdateMetadata());
*/
