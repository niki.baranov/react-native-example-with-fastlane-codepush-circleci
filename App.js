/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import codePush from 'react-native-code-push';
import React, {useState, useEffect} from 'react';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  Image,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const App: () => React$Node = () => {
  const [show, setShow] = useState(false);
  const [metadata, setMetadata] = useState();
  const [conf, setConf] = useState();

  useEffect(() => {
    // Get latest update info
    const getMetadata = async () => {
      const metadata = await codePush.getUpdateMetadata();
      setMetadata(metadata);
    };
    getMetadata();

    const getConf = async () => {
      const conf = await codePush.getConfiguration();
      setConf(conf);
    };
    getConf();
  });

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          testID="welcome"
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <Button
            title="The Button from GitLab v10.12.2019"
            onPress={() => setShow(!show)}
            testID="hello_button"
          />
          {show && (
            <Image
              source={require('./android/app/src/img/shareway.jpg')}
              style={{width: null, height: 400}}
              testID="shareway-kuva"
            />
          )}
          {show && (
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>
                Codepush update version info
              </Text>
              {conf && (
                <Text style={styles.sectionTitle}>
                  Current app version: {conf.appVersion}
                  {'\n'}
                </Text>
              )}
              {metadata && (
                <Text style={styles.sectionTitle}>
                  Update app version: {metadata.appVersion}
                  {'\n'}
                  Current update version: {metadata.label}
                  {'\n'}
                </Text>
              )}
            </View>
          )}
          <Header />
          {global.HermesInternal == null ? null : (
            <View style={styles.engine}>
              <Text style={styles.footer}>Engine: Hermes</Text>
            </View>
          )}
          <View style={styles.body}>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>Step One</Text>
              <Text style={styles.sectionDescription}>
                Edit <Text style={styles.highlight}>App.js</Text> to change this
                screen and then come back to see your edits.
              </Text>
            </View>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>See Your Changes</Text>
              <Text style={styles.sectionDescription}>
                <ReloadInstructions />
              </Text>
            </View>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>Debug</Text>
              <Text style={styles.sectionDescription}>
                <DebugInstructions />
              </Text>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
